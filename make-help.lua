local subdir = arg[1]
local input = io.read ("a"):gsub ("\\\n", "").." "
local deps = {}
local root = input:match ("^([^:]+):")
for dep in input:gmatch ("%s(%S+)") do
	table.insert (deps, dep)
end
io.write (subdir, root, " : ", table.concat (deps, " "), "\n")
for _, dep in ipairs (deps) do
	io.write (subdir..dep, " :\n")
end

