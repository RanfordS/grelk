#include "Perlin.h"
//~@>	Perlin.c   <@~//
//	credit to https://gist.github.com/nowl/828013

const uint8_t perlinhash[256] = {
	215, 100, 199, 204, 233,  49,  84, 194,
	 69, 140, 121, 160,  92, 130, 244, 234,
	161, 181,  33, 152,   3,  60,  34, 206,
	 39, 102,  30,  25, 255,  56, 131, 214,
	156,  76, 164, 133, 124, 249,  73, 196,
	134, 195,  99, 227,  70,  88, 207, 236,
	 15, 242, 132,  19,  47, 169, 225,  87,
	 13,   4, 115,  16,  61, 248, 229, 216,
	 67, 139,  96, 189, 128, 170, 136,   9,
	110, 238, 237, 180,  71, 185, 159,  86,
	174,  41, 109, 222, 209,  80,  53, 224,
	 85, 172, 245, 147, 163, 213, 107, 232,
	 95, 201, 168, 230, 116,  46, 243, 228,
	 31, 218, 153, 103, 148,  62, 190,  66,
	106,  48,  38,  59, 137,  98,  35, 226,
	 21,  26, 122, 187, 252, 239, 166,  90,
	182,  82,  65,  51, 144,  54,  28, 179,
	 23, 197,  36, 184,  11, 246,   7, 126,
	 42,  58, 198, 177, 157, 250, 151, 186,
	 17,  27, 125,  12,  10,  55, 117, 205,
	143, 188,   6,  43, 254,  57, 220,  32,
	253,   8, 211,  20,   0, 223, 145,  63,
	 52, 105, 221, 200,  97, 138, 142, 114,
	167,  29, 129, 183,  81, 251, 146, 212,
	175, 150,   5, 176, 202, 231, 208, 192,
	241, 158, 210, 247, 127, 104,  44, 173,
	193,  18, 120,  72, 119,  64, 149,  77,
	 79,  68,  14, 135,  74, 141, 111, 240,
	 75, 112, 162,  24, 101, 118, 219,  93,
	 83, 191,  91,   1,  78, 154, 178,  40,
	155, 108,  94,  45, 171,  37, 165,   2,
	113, 203, 217, 123,  50,  22, 235,  89};

__attribute__((const))
float lerp(float a, float b, float c)
{
	return a*(1-c) + b*c;
}

__attribute__((const))
float serp(float x, float y, float s)
{
    return lerp(x, y, s * s * (3-2*s));
}

__attribute__((pure))
uint8_t noise2(uint8_t x, uint8_t y)
{
	uint8_t tmp = perlinhash[y];
	return perlinhash[(tmp + x) % 256];
}

__attribute__((const))
float noise2d(float x, float y)
{
	//	obtain integer and floating parts
    int x_int = x;
    int y_int = y;
    float x_frac = x - x_int;
    float y_frac = y - y_int;
	//	sample noise
    uint8_t s = noise2(x_int, y_int);
    uint8_t t = noise2(x_int+1, y_int);
    uint8_t u = noise2(x_int, y_int+1);
    uint8_t v = noise2(x_int+1, y_int+1);
	//	interp samples
    float low = serp(s, t, x_frac);
    float high = serp(u, v, x_frac);
    return serp(low, high, y_frac);
}

__attribute__((const))
float perlin2d(float x, float y, float freq, float amp)
{
	float xa = x*freq;
	float ya = y*freq;
	float res = 0;
	float div = 0.0;
	
	for(int i=0; i<PERLDEPTH; i++)
	{
		div += 256 * amp;
		res += noise2d(xa, ya) * amp;
		amp /= 2;
		xa *= 2;
		ya *= 2;
	}
	return res/div;
}
