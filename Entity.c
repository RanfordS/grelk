#include "Entity.h"

GLuint eVBO = 0;
GLuint eIBO = 0;

rglQuadrangle EntityGeometry =
{.A = {.x=-0.75,	.y=0.0,		.s=0.0,		.t=1.0}
,.B = {.x= 0.25,	.y=0.0,		.s=1.0,		.t=1.0}
,.C = {.x= 0.25,	.y=1.0,		.s=1.0,		.t=0.0}
,.D = {.x=-0.75,	.y=1.0,		.s=0.0,		.t=0.0}
};
GLuint EntityIndex[4] = {0,1,3,2};

GLuint EntityTextures[et_num];
char EntityTexturePaths[et_num][32] =
{	[et_soli]		= "tmpgrfx/Soli.png"
,	[et_brawler]	= "tmpgrfx/NotSoli.png"
};

const EntityProperty EntityProperties[et_num] =
{	[et_soli] =
	{	.size = {.x=0.5*(16.0/13.0), .y=0.5*(16.0/13.0)}
	,	.radius = R3O2/5
	}
,	[et_brawler] =
	{	.size = {.x=0.5*(16.0/13.0), .y=0.5*(16.0/13.0)}
	,	.radius = R3O2/5
	}
};

M3 entsqew={.m={
	1.0f, 0.5f, 0.0f,
	0.0f, R3O2, 0.0f,
	0.0f, 0.0f, 1.0f}};


void initentity(void)
{
	printf("[ ]:\tinitentity\n");
	
	glGenBuffers(1, &eVBO);
	glBindBuffer(GL_ARRAY_BUFFER, eVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rglQuadrangle), (GLfloat*)&EntityGeometry, GL_STATIC_DRAW);
	
	glGenBuffers(1, &eIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4*sizeof(GLuint), EntityIndex, GL_STATIC_DRAW);
	
	printf("[?]:\teVBO = %i, eIBO = %i\nentsqew =\n", eVBO, eIBO);
	M3Print(entsqew);
}

//~@> New form

size_t EntityBounds[et_num];

uint8_t	EntityTileLock		[MaxEntities/8];
int		EntityTileLocked	[MaxEntities];
int		EntityTileLockedNum;
int		EntityTileFree		[MaxEntities];
int		EntityTileFreeNum;

size_t	EntityTileX			[MaxEntities];
size_t	EntityTileY			[MaxEntities];
V2		EntityAcceleration	[MaxEntities];
V2		EntityVelocity		[MaxEntities];
V2		EntityPosition		[MaxEntities];
V2		EntityCandidate		[MaxEntities];
V2		EntityVisualPosition[MaxEntities];
float	EntityVisualAngle	[MaxEntities];

const V2 U = {.x= 0.0, .y= SQRT3/3};
const V2 L = {.x=-0.5, .y=-SQRT3/6};
const V2 R = {.x= 0.5, .y=-SQRT3/6};

float size_t_floatDiff(size_t a, size_t b)
{
	bool neg = a < b;
	return (float)(neg ? b-a : a-b) * (1.0f - 2*neg);
}

void GeneralPhysicsPass(void)
{
	//	apply acceleration, generate candidate positions and sort by tile lock
	for(int i=0; i < MaxEntities; i++)
	{
		aV2Add(EntityVelocity[i], V2Scale(EntityAcceleration[i], physdt));
		EntityCandidate[i] = V2Add(EntityPosition[i], V2Scale(EntityVelocity[i], physdt));
		
		EntityTileLockedNum = 0;
		EntityTileFreeNum = 0;
		
		size_t id = i/8; uint8_t sid = i%8;
		if((EntityTileLock[id]>>sid)&1)
			EntityTileLocked[EntityTileLockedNum++] = i;
		else
			EntityTileFree[EntityTileFreeNum++] = i;
	}
	EntityType entType = 0;
	EntityProperty entdata = EntityProperties[entType];
	//	parse free
	for(int e=0; e < EntityTileFreeNum; e++)
	{
		int id = EntityTileFree[e];
		//	maintain correct entity reference
		while(!(id<EntityBounds[entType+1]))
			entdata = EntityProperties[++entType];
		
		size_t	entTX = EntityTileX[id];
		size_t	entTY = EntityTileY[id];
		bool	entUp = Upness(entTX,entTY);
		
		//	collision detection
		float candidateTime = 2;
		size_t candidateTX, candidateTY;
		V2 candidatePos;
		V2 candidateTangent;
		
		for(int rx = -2; rx < 3; rx++)
		{
			size_t x = EntityTileX[id] + rx;
			for(int ry = -1; ry < 2; ry++)
			{
				//	tile data
				size_t			y = EntityTileY[id] + ry;
				uint16_t	 tile = CurrentLevelTile(x,y);
				TileOrient orient = tile & to_m;
							tile >>= TT_Offset;
				tt_slup	   lookup = TileTypeSegmentLookup[tile];
				bool 	   tileUp = Upness(x,y);
				//	relative angle
				bool		  inv = TileInvert[orient];
				float		theta = TileAngle [orient] + M_PI*tileUp;
				float	   thetac = cosf(theta);
				float	   thetas = sinf(theta);
				//	relative position
				V2 rel =	{  .x = 0.5f*size_t_floatDiff(entTX, x)
							,  .y = R3O2*size_t_floatDiff(entTY, y) + (SQRT3/6)*(tileUp - entUp)
							};
				V2 entPos0 = V2RotatePreComp(V2Add(rel, EntityPosition [id]), thetac, thetas);
				V2 entPos1 = V2RotatePreComp(V2Add(rel, EntityCandidate[id]), thetac, thetas);
				
				for(int i=0; i < lookup.num; i++)
				{
					TilePoint seg = TileSegments[lookup.off + i];
					switch(seg.type)
					{
						case teqt_linear:
						{
							float sign = 1 - 2*inv;
							V2 v = V2Sub(entPos1, entPos0);
							V2 d = V2Scale(seg.linear.direction, sign);
							V2 n = {.x = -d.y, .y = d.x};
							float delta = V2Dot(v,n);
							//	correct direction of travel
							if(delta>=0) break;
							
							V2 alpha = V2Scale(V2Sub(V2Add(seg.position, V2Scale(n, entdata.radius)), entPos0), 1/delta);
							
							float t = d.y*alpha.x - d.x*alpha.y;
							//	within the time frame, better than the candidate
							if(!((0<t)&&(t<candidateTime))) break;
							
							float lambda = v.y*alpha.x - v.x*alpha.y;
							//	and hit the segment
							if(fabsf(lambda)>seg.linear.halflength) break;
							
							candidateTX = x;
							candidateTY = y;
							candidatePos.x = lambda; //	from angle to distance
							candidatePos.y = i;
							candidateTangent = d;
							
							break;
						}
						case teqt_radial:
						{
							float sign = 1 - 2*(seg.radial.inside^inv);
							V2 v = V2Sub(entPos1, entPos0);
							V2 d = V2Sub(seg.position, entPos0);
							float alpha = V2Dot(v,v);
							float  beta = V2Dot(d,v);
							float     r = seg.radial.radius + sign*entdata.radius;
							float gamma = V2Dot(d,d) - r*r;
							float delta = beta*beta - alpha*gamma;
							//	does collision occur
							if(delta<0) break;
							
							float t = beta - sign*sqrtf(delta);
							//	within the time frame, better than the candidate
							if(!((0<t)&&(t<candidateTime))) break;
							
							V2 pt = V2Rotate(V2Add(entPos0, V2Scale(v,t)), -seg.radial.midAngle);
							float lambda = atan2f(pt.y, pt.x);
							//	and hit the segment
							if(fabsf(lambda)>seg.radial.angleRange) break;
							
							candidateTX = x;
							candidateTY = y;
							candidatePos.x = lambda*r; //	from angle to distance
							candidatePos.y = i;
							candidateTangent = V2Polar(lambda + seg.radial.midAngle - sign*M_PI/2, 1);
							
							break;
						}
						default:
							break;
					}
				}
			}
		}
		
		//	use valid candidate
		if(candidateTime<=1)
		{
			EntityTileX		[id]   = candidateTX;
			EntityTileY		[id]   = candidateTY;
			EntityCandidate	[id]   = candidatePos;
			EntityVelocity	[id].x = V2Dot(EntityVelocity[id], candidateTangent);
			EntityVelocity	[id].y = 0;
			
			uint8_t sid = id%8;
			id /= 8;
			
			EntityTileLock[id] |= 1<<sid;
		}
		else	//	tile relativity
		{
			int inc = 1 - 2*Upness(EntityTileX[id],EntityTileY[id]);
			V2 p = EntityCandidate[id];
			aV2Scale(p, inc);
			//	crawling loop
			do
			{	doif(V2Dot(p, U) > 1/3)
				{	EntityTileY[id] += inc;
					aV2Sub(p, U);
				}
				elif(V2Dot(p, L) > 1/3)
				{	EntityTileX[id] -= inc;
					aV2Sub(p, L);
				}
				elif(V2Dot(p, R) > 1/3)
				{	EntityTileX[id] += inc;
					aV2Sub(p, R);
				}
				else break;
				//	alternating upness tracker
				inc = -inc;
				aV2Scale(p, -1);
				
			} while(true);
			
			EntityCandidate[id] = V2Scale(p, inc);
		}
	}
	for(int e=0; e < EntityTileLockedNum; e++)
	{
		int id = EntityTileLocked[e];
		//	maintain correct entity reference
		while(!(id<EntityBounds[entType+1]))
			entdata = EntityProperties[++entType];
		
		//	relavent data
		do
		{
			V2		   entPos = EntityCandidate[id];
			size_t			x = EntityTileX[id];
			size_t			y = EntityTileY[id];
			uint16_t	 tile = CurrentLevelTile(x,y);
			TileOrient orient = tile & to_m; tile >>= TT_Offset;
			tt_slup	   lookup = TileTypeSegmentLookup[tile];
			bool 	   tileUp = Upness(x,y);
			TilePoint	  seg = TileSegments[lookup.off + (int)entPos.y];
			bool	 	  inv = TileInvert[orient];
			
			float limit;
			int sign;
			
			switch(seg.type)
			{
				case teqt_linear:
					sign = 1 - 2*inv;
					limit = seg.linear.halflength;
					break;
					
				case teqt_radial:
					sign = 1 - 2*(seg.radial.inside^inv);
					limit = seg.radial.angleRange*(seg.radial.radius + sign*entdata.radius);
					break;
					
				default:
					sign = 0;
					limit = 0;
					break;
			}
				
			if(fabsf(entPos.x)<=limit) break; // remains within segment
			
			int upaug = 1 - 2*tileUp;
			
			int side = 1 - 2*(entPos.x*sign < 0);	//pd
			
			entPos.x -= side*limit;
			entPos.y += sign*side;
			
			doif(entPos.y < 0)	//	td test
			{
				switch(orient)
				{
					case to_b: case to_bi:
						x -= upaug;
						break;
					
					case to_l: case to_li:
						y += upaug;
						break;
					
					case to_r: case to_ri:
						x += upaug;
						break;
					
					default: break;
				}
				  tile = CurrentLevelTile(x,y);
				orient = tile & to_m; tile >>= TT_Offset;
				lookup = TileTypeSegmentLookup[tile];
				
				Player.pos.y = sign < 0 ? 0 : TileTypeSegmentLookup[tile].num-1;
			}
			elif(entPos.y>=lookup.num)
			{
				switch(orient)
				{
					case to_b: case to_bi:
						x += upaug;
						break;
					
					case to_l: case to_li:
						x -= upaug;
						break;
					
					case to_r: case to_ri:
						y += upaug;
						break;
					
					default: break;
				}
				  tile = CurrentLevelTile(x,y);
				orient = tile & to_m; tile >>= TT_Offset;
				lookup = TileTypeSegmentLookup[tile];
				
				entPos.y = sign > 0 ? 0 : TileTypeSegmentLookup[tile].num-1;
			}
			seg = TileSegments[lookup.off + (int)entPos.y];
			float seglength;
			
			switch(seg.type)
			{
				case teqt_linear:
					seglength = seg.linear.halflength;
					break;
					
				case teqt_radial:
					seglength = seg.radial.angleRange*(seg.radial.radius + sign*entdata.radius);
					break;
					
				default:
					seglength = 0;
					break;
			}
			entPos.x -= side*seglength;
			//	write data back
			EntityCandidate	[id] = entPos;
			EntityTileX		[id] = x;
			EntityTileY		[id] = y;
		}
		while(true);
	}
	//	use candidate
	memcpy(EntityPosition, EntityCandidate, MaxEntities*sizeof(V2));
}

void GenerateVisualPositions(void)
{
	EntityTileLockedNum = 0;
	EntityTileFreeNum = 0;
	//	sort
	for(size_t i = 0; i < MaxEntities; i++)
	{
		size_t id = i/8; uint8_t sid = i%8;
		if((EntityTileLock[id]>>sid)&1)
			EntityTileLocked[EntityTileLockedNum++] = i;
		else
			EntityTileFree[EntityTileFreeNum++] = i;
	}
}
