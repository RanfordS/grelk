#include "Core.h"
#include "Input.h"
#include "Draw.h"
#include "Entity.h"
#include "World.h"

//	TODO: Move include Core.h into the headers.
double time = 0;
uint8_t incer = 0;
double acc = 0;

float r3o2;

int main(int argc, char** argv)
{
	(void) argc; (void) argv;
	
	r3o2 = sqrt(3)/2;
	
	printf("r3o2 = %f\n", r3o2);
	
	if(!GraphicsInit())
	{
		printf("!!!:\tInitialisation failed\n");
		return -1;
	}
	
	ReadBinds(NULL);
	loadlevel(0);
	initentity();
	
	while(DoEvents())
	{
		time+=dt;
		acc+=dt;
		if(++incer == 0)
		{	printf("~dt: %f (%.2ffps)\n", acc / 256, 256 / acc);
			acc = 0;
		}
		//glClearColor(0, 0, fabs(sinf(time)), 0);
		ProcessPlayer();
		Draw();
	}
	
	//	do any saving that still has not been done
	
	GraphicsQuit();
	return 0;
}
