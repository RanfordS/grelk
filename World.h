#pragma once
#include "Core.h"
#include "Draw.h"
//~@>	World.h   <@~//

//!	Tile Orientation flags, can be composed to denote the orientation and flip of a tile.
typedef enum
{	to_b = 1 << 0		//!< Tile orientation bottom.
,	to_l = 1 << 1		//!< Tile orientation left.
,	to_r = 1 << 2		//!< Tile orientation right.
,	to_bi = to_l | to_r	//!< Tile orientation bottom inverse.
,	to_li = to_b | to_r	//!< Tile orientation left inverse.
,	to_ri = to_b | to_l	//!< Tile orientation right inverse.
,	to_m = 7			//!< Tile orientation mask.
} TileOrient;

//!	The number of bits by which the tile type is offset by.
#define TT_Offset 3
//!	A macro which converts tile and orientation data into the internal format.
#define TileFormat(tile,orient) (((tile)<<TT_Offset)|(orient))

//! Tile Type Enumeration. Currently a uint16_t, with tile orientation taken into account, this allows for 4095 different tiles.
typedef enum
{	tt_air		//!< No tile. Orientation data is used for invisible walls

,	tt_flat		//!< Simple flat tile (000, 060, 120, 180, 240, 300).

,	tt_sharp	//!< Trough and crest.

,	tt_sconl	//!< Left on ramp for a small curve.
,	tt_sc		//!< Small curve.
,	tt_sconr	//!< right on ramp for a small curve.
//	end of reduced set
,	tt_lconl	//!< On ramp for a large curve.
,	tt_lcsegl	//!< Large curve segment.
,	tt_lc		//!< Large curve.
,	tt_lcsegr	//!< Large curve segment.
,	tt_lconr	//!< On ramp for a large curve.

,	tt_sqew		//!< Simple flat slanted tile (030, 090, 150, 210, 270, 330).
,	tt_num		//!< Number of tile types.
} TileType;

typedef enum
{	wt_themeless		//!< Debugging world theme.
,	wt_moonlight		//!< Moonlight (S-Nilor).
,	wt_coast			//!< Nerri Coast (Comerell).
,	wt_ocean			//!< The Depths (Terential).
,	wt_desert			//!< Sunrise Sands (Lulilatan).
,	wt_crystal			//!< Decayde (Arthan & Etheri).
,	wt_skyline			//!< Tennor Skyrise (V-Sev).
,	wt_digital			//!< Techie Golden (Retech).
,	wt_relent			//!< Restless Zone (RelentorCeli).
} WorldTheme;

//!	The level data.
typedef struct
{	WorldTheme theme;	//!< Tile set.
	size_t width;		//!< Level width.
	size_t height;		//!< Level height.
	uint16_t* tile;		//!< Stride offset 3d tile array.
} Level;

//!	The graphical data of the level. Needed only by the draw code.
typedef struct
{	size_t chunksx;		//!< Width of the level in chucks.
	size_t chunksy;		//!< Height of the level in chunks.
	GLuint* VBOs;		//!< Vertex buffer objects.
	size_t* VBOSizes;	//!< Size of respective buffer object.
} LevelGeometry;

typedef enum
{	teqt_linear
,	teqt_radial
,	teqt_num
} TileEquationType;

typedef struct
{	uint8_t num;
	uint16_t off;
} tt_slup;

extern const tt_slup TileTypeSegmentLookup[tt_num];
//!	TODO: The descriptons of this structure and the surrounding systems have yet to be reformed!
typedef struct
{	TileEquationType type;	//!< The type of equation that the segment uses.
	V2 position;			//!< The origin point of the segment.
	float halflength;		//!< A value representing half the length of the segment.
	union
	{
		struct
		{	V2 direction;	//!< The normalised direction vector of the surface.
			float angle;	//!< The angle of the surface.
			float halflength;
		} linear;			//!< Structure containing data relavent to teqt_linear.
		struct
		{	float midAngle;	//!< The clockwise angle from the y axis at which the midpoint of the surface lies.
			float angleRange;
			float radius;	//!< The radius of the circle that forms the surface.
			bool inside;	//!< Whether or not the active surface is the inside one.
		} radial;			//!< Structure containing data relavent to teqt_radial.
	};
} TilePoint;

extern TilePoint TilePositionData[tt_num][2];

extern rglTriangle* Geometry;
extern GLuint* GeomIndex;
extern GLuint VBO;
extern GLuint IBO;

extern Level CurrentLevel;
extern LevelGeometry CurrentLevelGeometry;
#define CurrentLevelTile(x,y) CurrentLevel.tile[CurrentLevel.width*(y) + (x)]

extern const TilePoint TileSegments[];

extern const float TileAngle[to_m];
extern const bool TileInvert[to_m];

extern void loadlevel(size_t id);

#define getSeg(tiletype,subseg) TileSegments[TileTypeSegmentLookup[tiletype].off+(subseg)]
extern V2 getVisualPos(V2 pos, size_t tr, size_t tc, float* theta, bool makeGlobal, bool tileLock, bool dbg);
