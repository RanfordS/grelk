#pragma once
#include "Core.h"
//~@>	Input.h   <@~//
#define Mappings 3	//!	The number of mappings each action has
#define BindingsFile "binds.ini"

//!	State (ks_) and masking (km_) options.
typedef enum
{	ks_up       = 0x00	//!<	Key is up but not this frame.
,	ks_down		= 0x01	//!<	Key is down but not this frame.
,	ks_released = 0x02	//!<	key was released this frame.
,	ks_pressed	= 0x03	//!<	Key was pressed this frame.
,	km_state	= 0x01	//!<	Keystate mask.
,	km_cstate	= 0x55	//!<	Clear state from block.
,	km_delta	= 0x02	//!<	Keystate delta mask.
,	km_cdelta	= 0xAA	//!<	Clear state delta from block.
,	km_unpack	= 0x03	//!<	Clear irrelevant data from block.
} KeyState;

//!	Actions.
typedef enum
{	a_left
,	a_right
,	a_up
,	a_down
,	a_jump
,	a_attack
,	a_num
} Action;

//!	Labels for Actions.
extern char ActionLabel[a_num][8];

typedef struct
{	SDL_Scancode key;
	SDL_Scancode mod;
} Bind;

//!	The end result of the action mapping.
extern Bind Bindings[Mappings * a_num];
extern Uint8 Actions[a_num];
extern Uint32 tick;
extern M3 aspectmat;

//~@>	Binds.c   <@~//
extern bool ReadBinds(char* path);
extern char ActionLabel[a_num][8];

//~@>	Debug   <@~//
extern bool fixdt;
