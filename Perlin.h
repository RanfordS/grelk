#pragma once
#include "Core.h"
//~@>	Perlin.h   <@~//
#ifndef PERLDEPTH
#define PERLDEPTH 2
#endif

extern float perlin2d(float x, float y, float freq, float amp);
