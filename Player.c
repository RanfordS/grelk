#include "Entity.h"
#include "Input.h"

float pxa  =  0.5f;// 1.0f;	//!	Player x acceration.
float pjs  =  2.5f;// 2.5f;	//!	Player jump speed.
float gvt  = -2.5f;//-2.5f;	//!	Gravity.
float brk  =  0.8f;// 1.5f;	//!	Breaking force.
float ltcb = 0.70f;// 0.7f;	//!	Breaking latch speed.
float ltci = 0.50f;// 0.5f;	//!	Initial latch speed.
float paxa = 0.45f;// 0.9f;	//!	Player air x acccerleration

Soli Player =
{	.pos = {.x=0, .y=0}
,	.tx = 2
,	.ty = 1
,	.health = 12
,	.flags = sf_lock | sf_djump
};

#define Sign(a) ((a) > 0 ? 1 : (a) < 0 ? -1 : 0)

__attribute__((const))
inline float maxf(float a, float b)
{
	return a > b ? a : b;
}

#define DecZeroClamp(var,b) var = maxf((var)-(b), 0)

struct bitstr
{
	char s[33];
};

struct bitstr createbitstr(uint32_t a)
{
	struct bitstr r = {};
	for(int i=16; i;)
	{
		r.s[--i] = a&1 ? '1' : '0';
		a>>=1;
	}
	return r;
}

float sztSubToF(size_t a, size_t b)
{
	bool neg = a < b;
	return (float)(neg ? b-a : a-b) * (neg ? -1.0f : 1.0f);
}

void ProcessPlayer(void)
{
	{
		//struct bitstr r = createbitstr(Player.flags);
		//printf("(%zi)flags %s, health %i\n", sizeof(SoliFlag)*CHAR_BIT, r.s, Player.health);
	}
	float drag = powf(0.85, dt);
	{	//	horizontal control logics
		int currentdir = Sign(Player.vel.x);
		
		bool left  = (Actions[a_left ]&km_state) == ks_down;
		bool right = (Actions[a_right]&km_state) == ks_down;
		if(Player.flags & sf_lock)	//	ground control
		{
			if(left && right)
			{
				switch(currentdir)
				{
					case -1:
						Player.vel.x += brk*dt;
						break;
					case  1:
						Player.vel.x -= brk*dt;
						break;
				}
				if(Sign(Player.vel.x)!=currentdir)
					Player.vel.x = 0;
			}
			else if(left)
			{
				if(Player.vel.x >= ltcb)
					Player.vel.x -= brk*dt;
				else
				{
					if(Player.vel.x > -ltci)
						Player.vel.x = -ltci;
					Player.vel.x -= pxa*dt;
				}
			}
			else if(right)
			{
				if(Player.vel.x <= -ltcb)
					Player.vel.x += brk*dt;
				else
				{
					if(Player.vel.x < ltci)
						Player.vel.x = ltci;
					Player.vel.x += pxa*dt;
				}
			}
			else
			{
				if(fabsf(Player.vel.x) > ltcb)
					Player.vel.x -= brk*dt*Sign(Player.vel.x);
				
				if(fabsf(Player.vel.x) <= ltcb)
					Player.vel.x = 0;
			}
		}
		else	//	air control
		{
			if(left)
				Player.vel.x -= paxa*dt;
			if(right)
				Player.vel.x += paxa*dt;
		}
	}
	{
		bool releaselock = false;
		
		if(Actions[a_jump] == ks_pressed)
		{
			printf("jump pressed\n");
			if(Player.flags & sf_lock)
			{	releaselock = true;
				Player.vel.y = pjs;
				if(Player.flags & sf_djump)
					Player.flags |= sf_djmpa;
			}
			else if(Player.flags & sf_djmpa)
			{
				Player.vel.y = pjs;
				Player.flags ^= sf_djmpa;
			}
		}
		
		if(releaselock)
		{
			printf("Lock release:\n");
			Player.flags &= ~sf_lock;
			Player.vel = V2Rotate(Player.vel, -Player.angle);
			Player.pos = getVisualPos(Player.pos, Player.ty, Player.tx, &Player.angle, false, true, true);
			V2 dir = V2Polar(M_PI/2 - Player.angle, EntityProperties[et_soli].radius);
			Player.pos = V2Add(Player.pos, dir);
			Player.angle = 0;
		}
		
		if(!(Player.flags & sf_lock))
		{
			Player.vel.y += gvt*dt;
		}
	}
	Player.vel = V2Scale(Player.vel, drag);
	
	if(Actions[a_up] == ks_pressed)
		Player.ty++;
	if(Actions[a_down] == ks_pressed)
		Player.ty--;
	
	
	DecZeroClamp(Camera.currentShake, dt);
	
//	if(Actions[a_attack] == ks_pressed)
//		Camera.currentShake = 1.0;
	
	//printf("(%5.2f,%5.2f)'(%5.2f,%5.2f)\n", Player.pos.x, Player.pos.y, Player.vel.x, Player.vel.y);
	
	Camera.actualShake = Camera.currentShake*Camera.currentShake;
	
	V2 oldPos = Player.pos;
	size_t oldtx = Player.tx;
	size_t oldty = Player.ty;
	Player.pos = V2Add(Player.pos, V2Scale(Player.vel, dt));
	
	//	tile handleing
	//*/
	while(true)
	{
		uint16_t tile = CurrentLevel.tile[16*Player.ty + Player.tx];
		TilePoint seg = getSeg(tile>>TT_Offset, (int)Player.pos.y);
		
		bool upness = (Player.ty ^ Player.tx) & 1;
		bool inv = false;
		switch(tile & to_m)
		{
			case to_bi:
			case to_li:
			case to_ri:
				inv = true;
			default:
				break;
		}
		
		//	fling off
		if(seg.type == teqt_radial)
			if(!seg.radial.inside ^ inv)
				if((fabsf(Player.vel.x) > 4*seg.radial.radius) && (Player.flags & sf_lock))
				{	printf("[!]:\tLatch lost\n");
					//Player.vel.x = 0;
					//releaselock = true;
				}
		
		if(Player.flags & sf_lock)
		{
			//	player delta, which side of the segment the player left
			int pd	= Player.pos.x >  seg.halflength ?  1
					: Player.pos.x < -seg.halflength ? -1 : 0;
			
			//	move to next segment
			if(pd)
			{
				Player.pos.x -= pd*seg.halflength;
				
				//	segment delta
				int sd = inv ? -pd : pd;
				Player.pos.y += sd;
				
				//	which direction did the player leave the tile
				int td = Player.pos.y >= TileTypeSegmentLookup[tile>>TT_Offset].num ?  1
						: Player.pos.y < 0 ? -1 : 0;
				
				//	move to next tile
				if(td)
				{	int upaug = 2*upness - 1;	//	augmented upness value
					switch(tile & to_m)
					{
						case to_bi:
						case to_b :
							if(td<0)
								Player.tx -= upaug;
							else
								Player.tx += upaug;
							break;
							
						case to_li:
						case to_l :
							if(td<0)
								Player.ty += upaug;
							else
								Player.tx -= upaug;
							break;
							
						case to_ri:
						case to_r :
							if(td<0)
								Player.tx += upaug;
							else
								Player.ty += upaug;
							break;
					}
					
					//	update tile reference
					if(inv)
						td*=-1;
					tile = CurrentLevel.tile[16*Player.ty + Player.tx];
					if(!tile)
						Player.flags &= ~sf_lock;
					Player.pos.y = td < 0 ? 0 : TileTypeSegmentLookup[tile>>TT_Offset].num-1;
				}
				//	update segment reference and correct player x pos
				seg = getSeg(tile>>TT_Offset, (int)roundf(Player.pos.y));
				Player.pos.x -= pd*seg.halflength;
			}
			else
			{
				break;
			}
		}
		else
		{
			int upaug = 2*upness - 1;
			V2 prox = Player.pos;
			
			//	adjust for upness
			if(upness)
				prox = V2Scale(prox, -1);
			
			bool td = true;
			//	NOTE:	uses upness=0 as base case, which is inconsistent with
			//			how upness is usually based
			if(2*prox.y*SQRT3 < -1)					//	flat edge
			{	prox.y += SQRT3/3.0f;
				printf("\tQf\n");
				printf("\ty: %zi ->", Player.ty);
				Player.ty += upaug;
				printf("\t%zi\n", Player.ty);
			}
			else if(SQRT3*prox.y - 3*prox.x > 1)	//	left edge
			{	prox.x += 0.5f;
				prox.y -= SQRT3/6.0f;
				printf("\tQl\n");
				printf("\tx: %zi ->", Player.tx);
				Player.tx += upaug;
				printf("\t%zi\n", Player.tx);
			}
			else if(SQRT3*prox.y + 3*prox.x > 1)	//	right edge
			{	prox.x -= 0.5f;
				prox.y -= SQRT3/6.0f;
				printf("\tQr\n");
				printf("\tx: %zi ->", Player.tx);
				Player.tx -= upaug;
				printf("\t%zi\n", Player.tx);
			}
			else									//	no change
				td = false;
			
			//	restore upness
			if(upness)
				prox = V2Scale(prox, -1);
			
			if(td)
			{
				Player.pos = prox;
				printf("\t(%5.2f,%5.2f) -> (%5.2f,%5.2f)\n", oldPos.x, oldPos.y, Player.pos.x, Player.pos.y);
				upness ^= true;
				//	recurse to check for addition tile changes
			}
			else
				break;
		}
	}
	//*/
	//	tile locking
	if(!(Player.flags & sf_lock))
	{
		//	oldPos -> Player.pos
		//	oldtx -> Player.tx, oldty -> Player.ty
		//	potential tile range
		size_t xl = mMin(oldtx, Player.tx);
		xl = mMax(xl,2)-2;
		size_t xu = mMax(oldtx, Player.tx);
		xu = mMin(xu,13)+2;
		size_t yl = mMin(oldty, Player.ty);
		yl = mMax(yl,1)-1;
		size_t yu = mMax(oldty, Player.ty);
		yu = mMin(yu,7)+1;
		
		system("clear");
		printf("range ([%2zi,%2zi],[%2zi,%2zi])\n", xl, xu, yl, yu);
		
		bool upness0 = (oldtx^oldty)&1;
		bool upness1 = (Player.tx^Player.ty)&1;
		
		
		oldPos.x -= 0.5*sztSubToF(Player.tx, oldtx);
		oldPos.y -= R3O2*sztSubToF(Player.ty, oldty) + (SQRT3/6)*((upness1^upness0)==0 ? 0 : upness0==0 ? 1 : -1);
		printf("%zi, %zi -> %zi, %zi\n", oldtx, oldty, Player.tx, Player.ty);
		printf("%f, %f -> %f, %f\n", oldPos.x, oldPos.y, Player.pos.x, Player.pos.y);
		
		float t = 2;
		size_t ltx, lty;
		float lpx, lpy;
		float lang;
		
		for(size_t x=xl; x<=xu; x++)
			for(size_t y=yl; y<=yu; y++)
			{
				printf("collision checking x=%3zi, y=%3zi\n", x, y);
				bool upness = (x^y)&1;
				//	make position relative
				V2 rel =
				{	.x = 0.5*sztSubToF(Player.tx, x)
				,	.y = R3O2*sztSubToF(Player.ty, y) + (SQRT3/6)*((upness1^upness)==0 ? 0 : upness==0 ? 1 : -1)
				};
				V2 px0 = V2Add(rel, oldPos);
				V2 px1 = V2Add(rel, Player.pos);
				//	tile collision bound
				/*if(V2MagSquared(V2Min(px0, px1)) > 1)
				{	printf("failed on collision bound check\n");
					continue;
				}*/
				//	get tile data
				uint16_t tile = CurrentLevel.tile[16*y + x];
				TileOrient orient = tile & to_m;
				tile >>= TT_Offset;
				tt_slup lookup = TileTypeSegmentLookup[tile];
				//	correct for orientation
				float theta = upness ? 0 : M_PI;
				bool inv = false;
				switch(orient)
				{
					case to_bi:
						inv = true;
					case to_b :
					default   :
						break;
						
					case to_li:
						inv = true;
					case to_l :
						theta += 2*M_PI/3;
						break;
						
					case to_ri:
						inv = true;
					case to_r :
						theta -= 2*M_PI/3;
						break;
				}
				float thetac = cosf(theta);
				float thetas = sinf(theta);
				//if(!upness)
				//	theta -= M_PI;
				aV2RotatePreComp(px0, thetac, thetas);
				aV2RotatePreComp(px1, thetac, thetas);
				V2 pxd = V2Sub(px1, px0);
				
				for(int i = 0; i<lookup.num; i++)
				{	TilePoint seg = TileSegments[lookup.off + i];
					printf("segment %i\n", i);
					switch(seg.type)
					{
						case teqt_linear:
						{	//	setup variables
							V2 dir = seg.linear.direction;
							V2 norm = {.x = -dir.y, .y = dir.x};
							if(inv)
								aV2Scale(norm, -1);
							//	side test
							float denom = V2Dot(norm, pxd);
							if(denom >= 0)
							{	if(x==3) printf("failed dot test: %.2f\n", denom);
								continue;
							}
							//	"constant" term
							V2 cv = V2Sub(px0, V2Add(seg.position, V2Scale(norm, EntityProperties[et_soli].radius)));
							//	calculate t and test
							float tc = (dir.x*cv.y - dir.y*cv.x)/denom;
							if(tc<=0 || tc>1)
							{	if(x==3) printf("failed on time test, %f\n", tc);
								continue;
							}
							if(tc>t)
							{	if(x==3) printf("failed to beat candidate, %f\n", tc);
								continue;
							}
							//	calculate position and check
							float p = (cv.x*pxd.y - cv.y*pxd.x)/denom;
							if(fabsf(p) > seg.halflength)
							{	if(x==3) printf("failed on length test, %f\n", p);
								continue;
							}
							//	store result
							lang = -theta + seg.linear.angle;
							if(x==3) printf("candidate: t=%f, p=%f, denom=%f, angle=%f\n", tc, p, denom, lang);
							t = tc;
							ltx = x;
							lty = y;
							lpx = p;
							lpy = i;
							break;
						}
						case teqt_radial:
						{	//	use inside edge?
							bool ixi = inv ^ seg.radial.inside;
							int aixi = 2*ixi - 1;
							V2 dif = V2Sub(px0, seg.position);
							float radprox = seg.radial.radius - aixi*EntityProperties[et_soli].radius;
							//	sanity check, valid circle radius
							if(radprox<=0)
							{	printf("failed on radprox, %f\n", radprox);
								continue;
							}
							//	is collision
							float alpha = V2MagSquared(pxd);
							float beta  = V2Dot(pxd, dif);
							float radproxsqr = radprox*radprox;
							float gamma = V2MagSquared(dif) - radproxsqr;
							float deter = beta*beta - alpha*gamma;
							if(deter <= 0)
							{	printf("failed on deter, %f\n", deter);
								continue;
							}
							//	calculate collision times
							deter = sqrtf(deter);
							printf("deter = %f\n", deter);
							float d0 = V2DistanceSquared(seg.position, px0);
							float t0 = (-beta-deter)/alpha;
							bool in0 = radproxsqr > d0;
							float d1 = V2DistanceSquared(seg.position, px1);
							float t1 = (-beta+deter)/alpha;
							bool in1 = radproxsqr > d1;
							printf("preswp:\n\td0 = %+5.2f, d1 = %+5.2f\n", d0, d1);
							printf("\tt0 = %+5.2f, t1 = %+5.2f\n", t0, t1);
							printf("\tin0 = %1i, in0 = %1i\n", in0, in1);
							//	inside edge time flip
							if(ixi)
							{
								float sf = t0;
								t0 = t1;
								t1 = sf;
								sf = d0;
								d0 = d1;
								d1 = sf;
								bool sb = in0;
								in0 = in1;
								in1 = sb;
								printf("swapped\n");
							}
							
							printf("postswp:\n\td0 = %+5.2f, d1 = %+5.2f\n", d0, d1);
							printf("\tt0 = %+5.2f, t1 = %+5.2f\n", t0, t1);
							printf("\tin0 = %1i, in0 = %1i\n", in0, in1);
							
							if(in0)
							{	printf("failed on in0");
								continue;
							}
							float tc;
							
							if(!in1)
							{
								if(t0<=0)
									t0 = /*ixi ? -1 :*/ 2;
								if(t1<=0)
									t1 = /*ixi ? -1 :*/ 2;
								tc = /*ixi ? mMax(t0,t1) :*/ mMin(t0,t1);
								printf("flow in1 = 1, t = [%+5.2f,%+5.2f]\n", t0, t1);
							}
							else
							{
								tc = t0;
								printf("flow in1 = 0, tc = t0 = %+5.2f\n", t0);
							}
							//	this frame
							if(tc<=0 || tc>1)
							{
								printf("failed on time test\n");
								continue;
							}
							
							//	theta test
							V2 relpat = V2Add(px0, V2Scale(pxd, tc));	//	collision position
							aV2Sub(relpat, seg.position);	//	relative to segment center
							aV2Rotate(relpat, -M_PI/2 + seg.radial.midAngle);
							float ang = atan2f(relpat.y, relpat.x);
							if(fabsf(ang) > seg.halflength/seg.radial.radius)
							{
								printf("failed on angle test, %f\n", ang);
								continue;
							}
							printf("ang = %f\n", ang);
							
							printf("candidate: t=%f\n", tc);
							
							if(tc>t)
								continue;
							
							ang *= aixi;
							
							lang = ang;// + seg.radial.midAngle;
							printf("lang = %f\n", lang);
							t = tc;
							ltx = x;
							lty = y;
							lpx = ang*seg.radial.radius;
							lpy = i;
							
							break;
						}
						default:
							printf("Not equation implemented\n");
							break;
					}
				}
				printf("\n");
			}
			if(t<1.5f)
			{
				printf("locked to %zi, %zi\n", ltx, lty);
				Player.tx = ltx;
				Player.ty = lty;
				Player.pos.x = lpx;
				Player.pos.y = lpy;
				Player.flags |= sf_lock;
				
				printf("(%5.2f,%5.2f) >-(%5.2f)-> ", Player.vel.x, Player.vel.y, -lang);
				aV2Rotate(Player.vel, -lang);
				printf("(%5.2f,%5.2f)\n", Player.vel.x, Player.vel.y);
				Player.vel.y = 0;
			}
	}
	
	
	if(Player.tx > 15 || Player.ty > 7)
	{
		Player.pos.x = 0;
		Player.pos.y = 0;
		Player.tx = 7;
		Player.ty = 3;
		Player.flags &= ~sf_lock;
	}
	
	
	Player.vis = getVisualPos(Player.pos, Player.ty, Player.tx, &Player.angle, true, Player.flags & sf_lock, true);
	if(!(Player.flags & sf_lock))
		Player.vis.y -= EntityProperties[et_soli].radius;
	V2 offset = {.x=0, .y=0.46*R3O2};
	offset = V2Rotate(offset, -Player.angle);
	Camera.tar = V2Add(Player.vis, offset);
	Camera.targetScale = 1 + 0.5*log2f(maxf(1,V2Magnitude(Player.vel)));
	/*{
		struct bitstr str = createbitstr(CurrentLevel.tile[16*Player.ty + Player.tx]);
		printf("Current tile>> %s:%1i [%zi,%zi]\n", str.s, (int)(Player.ty ^ Player.tx)&1, Player.ty, Player.tx);
	}*/
	/*
	printf("pos %5.2f, %5.2f\n", Player.pos.x, Player.pos.y);
	printf("vis %5.2f, %5.2f\n", Player.vis.x, Player.vis.y);
	printf("tile %zi, %zi\n", Player.tx, Player.ty);
	printf("angle %5.2f\n", angle);
	printf("tar %5.2f, %5.2f\n", Camera.tar.x, Camera.tar.y);
	{
		uint16_t tile = CurrentLevel.tile[16*Player.ty + Player.tx];
		printf("seg %i\n", TileTypeSegmentLookup[tile>>TT_Offset].off+((int)Player.pos.y));
	}*/
}
