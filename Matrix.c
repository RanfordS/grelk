/*~@> Matrix.c <@~*/
#include "Matrix.h"

//	V2 generics

__attribute__((const))
inline V2 V2Add (V2 a, V2 b)
{
	V2 ret;
	for (int i = 0; i < 2; i++)
		ret.v[i] = a.v[i] + b.v[i];
	return ret;
}

__attribute__((const))
inline V2 V2Sub (V2 a, V2 b)
{
	V2 ret;
	for (int i = 0; i < 2; i++)
		ret.v[i] = a.v[i] - b.v[i];
	return ret;
}

__attribute__((const))
inline V2 V2Mul (V2 a, V2 b)
{
	V2 ret;
	for (int i = 0; i < 2; i++)
		ret.v[i] = a.v[i] * b.v[i];
	return ret;
}

__attribute__((const))
inline float V2Sum (V2 a)
{
	float ret = 0;
	for (int i = 0; i < 2; i++)
		ret += a.v[i];
	return ret;
}

__attribute__((const))
inline float V2Dot (V2 a, V2 b)
{
	return V2Sum(V2Mul(a, b));
}

__attribute__((const))
inline V2 V2Scale (V2 a, float b)
{
	V2 ret;
	for (int i = 0; i < 2; i++)
		ret.v[i] = a.v[i] * b;
	return ret;
}

__attribute__((const))
inline float V2Magnitude (V2 a)
{
	return sqrtf(V2Dot(a, a));
}

__attribute__((const))
inline V2 V2Normalise (V2 a)
{
	return V2Scale(a, 1.0f/V2Magnitude(a));
}

__attribute__((const))
inline float V2Distance (V2 a, V2 b)
{
	return V2Magnitude(V2Sub(a, b));
}

__attribute__((const))
inline V2 V2Direction (V2 a, V2 b)
{
	return V2Normalise(V2Sub(b, a));
}

__attribute__((const))
inline M2 M2MulF (M2 a, float b)
{
	M2 ret;
	for(int i = 0; i < 2*2; i++)
		ret.m[i] = a.m[i] * b;
	return ret;
}

__attribute__((const))
inline V2 M2MulV2 (M2 a, V2 b)
{
	V2 ret = {};
	for(int r = 0; r < 2; r++)
		for(int c = 0; c < 2; c++)
			ret.v[r] += M2ID(a,r,c) * b.v[c];
	return ret;
}

__attribute__((const))
inline M2 M2Mul (M2 a, M2 b)
{
	M2 ret = {};
	for(int r = 0; r < 2; r++)
		for(int c = 0; c < 2; c++)
			for(int i = 0; i < 2; i++)
				M2ID(ret,r,c) += M2ID(a,r,i) * M2ID(b,i,c);
	return ret;
}

__attribute__((const))
inline M2 M2Transpose (M2 a)
{
	M2 ret = {};
	for(int r = 0; r < 2; r++)
		for(int c = 0; c < 2; c++)
			M2ID(ret,r,c) += M2ID(a,c,r);
	return ret;
}

//	V3 generics

__attribute__((const))
inline V3 V3Add (V3 a, V3 b)
{
	V3 ret;
	for (int i = 0; i < 3; i++)
		ret.v[i] = a.v[i] + b.v[i];
	return ret;
}

__attribute__((const))
inline V3 V3Sub (V3 a, V3 b)
{
	V3 ret;
	for (int i = 0; i < 3; i++)
		ret.v[i] = a.v[i] - b.v[i];
	return ret;
}

__attribute__((const))
inline V3 V3Mul (V3 a, V3 b)
{
	V3 ret;
	for (int i = 0; i < 3; i++)
		ret.v[i] = a.v[i] * b.v[i];
	return ret;
}

__attribute__((const))
inline float V3Sum (V3 a)
{
	float ret = 0;
	for (int i = 0; i < 3; i++)
		ret += a.v[i];
	return ret;
}

__attribute__((const))
inline float V3Dot (V3 a, V3 b)
{
	return V3Sum(V3Mul(a, b));
}

__attribute__((const))
inline V3 V3Scale (V3 a, float b)
{
	V3 ret;
	for (int i = 0; i < 3; i++)
		ret.v[i] = a.v[i] * b;
	return ret;
}

__attribute__((const))
inline float V3Magnitude (V3 a)
{
	return sqrtf(V3Dot(a, a));
}

__attribute__((const))
inline V3 V3Normalise (V3 a)
{
	return V3Scale(a, 1.0f/V3Magnitude(a));
}

__attribute__((const))
inline float V3Distance (V3 a, V3 b)
{
	return V3Magnitude(V3Sub(a, b));
}

__attribute__((const))
inline V3 V3Direction (V3 a, V3 b)
{
	return V3Normalise(V3Sub(b, a));
}

__attribute__((const))
inline M3 M3MulF (M3 a, float b)
{
	M3 ret;
	for(int i = 0; i < 3*3; i++)
		ret.m[i] = a.m[i] * b;
	return ret;
}

__attribute__((const))
inline V3 M3MulV3 (M3 a, V3 b)
{
	V3 ret = {};
	for(int r = 0; r < 3; r++)
		for(int c = 0; c < 3; c++)
			ret.v[r] += M3ID(a,r,c) * b.v[c];
	return ret;
}

__attribute__((const))
inline M3 M3Mul (M3 a, M3 b)
{
	M3 ret = {};
	for(int r = 0; r < 3; r++)
		for(int c = 0; c < 3; c++)
			for(int i = 0; i < 3; i++)
				M3ID(ret,r,c) += M3ID(a,r,i) * M3ID(b,i,c);
	return ret;
}

__attribute__((const))
inline M3 M3Transpose (M3 a)
{
	M3 ret = {};
	for(int r = 0; r < 3; r++)
		for(int c = 0; c < 3; c++)
			M3ID(ret,r,c) += M3ID(a,c,r);
	return ret;
}

//	V4 generics

__attribute__((const))
inline V4 V4Add (V4 a, V4 b)
{
	V4 ret;
	for (int i = 0; i < 4; i++)
		ret.v[i] = a.v[i] + b.v[i];
	return ret;
}

__attribute__((const))
inline V4 V4Sub (V4 a, V4 b)
{
	V4 ret;
	for (int i = 0; i < 4; i++)
		ret.v[i] = a.v[i] - b.v[i];
	return ret;
}

__attribute__((const))
inline V4 V4Mul (V4 a, V4 b)
{
	V4 ret;
	for (int i = 0; i < 4; i++)
		ret.v[i] = a.v[i] * b.v[i];
	return ret;
}

__attribute__((const))
inline float V4Sum (V4 a)
{
	float ret = 0;
	for (int i = 0; i < 4; i++)
		ret += a.v[i];
	return ret;
}

__attribute__((const))
inline float V4Dot (V4 a, V4 b)
{
	return V4Sum(V4Mul(a, b));
}

__attribute__((const))
inline V4 V4Scale (V4 a, float b)
{
	V4 ret;
	for (int i = 0; i < 4; i++)
		ret.v[i] = a.v[i] * b;
	return ret;
}

__attribute__((const))
inline float V4Magnitude (V4 a)
{
	return sqrtf(V4Dot(a, a));
}

__attribute__((const))
inline V4 V4Normalise (V4 a)
{
	return V4Scale(a, 1.0f/V4Magnitude(a));
}

__attribute__((const))
inline float V4Distance (V4 a, V4 b)
{
	return V4Magnitude(V4Sub(a, b));
}

__attribute__((const))
inline V4 V4Direction (V4 a, V4 b)
{
	return V4Normalise(V4Sub(b, a));
}

__attribute__((const))
inline M4 M4MulF (M4 a, float b)
{
	M4 ret;
	for(int i = 0; i < 4*4; i++)
		ret.m[i] = a.m[i] * b;
	return ret;
}

__attribute__((const))
inline V4 M4MulV4 (M4 a, V4 b)
{
	V4 ret = {};
	for(int r = 0; r < 4; r++)
		for(int c = 0; c < 4; c++)
			ret.v[r] += M4ID(a,r,c) * b.v[c];
	return ret;
}

__attribute__((const))
inline M4 M4Mul (M4 a, M4 b)
{
	M4 ret = {};
	for(int r = 0; r < 4; r++)
		for(int c = 0; c < 4; c++)
			for(int i = 0; i < 4; i++)
				M4ID(ret,r,c) += M4ID(a,r,i) * M4ID(b,i,c);
	return ret;
}

__attribute__((const))
inline M4 M4Transpose (M4 a)
{
	M4 ret = {};
	for(int r = 0; r < 4; r++)
		for(int c = 0; c < 4; c++)
			M4ID(ret,r,c) += M4ID(a,c,r);
	return ret;
}



// Non-generics

inline V3 V2HomogeneousV3(V2 a)
{
	V3 ret = {
		.v = {a.x, a.y, 1.0f}};
	return ret;
}

inline V3 V3Cross(V3 a, V3 b)
{
	V3 ret = {
		.v = {
			a.y*b.z - a.z*b.y,
			a.z*b.x - a.x*b.z,
			a.x*b.y - a.y*b.x}
		};
	return ret;
}

V4 V3HomogeneousV4(V3 a)
{
	V4 ret = {
		.v = {a.x, a.y, a.z, 1.0f}};
	return ret;
}


__attribute__((const))
__attribute__((cold))
float M2Det (M2 a)
{
	return M2ID(a,0,0)*M2ID(a,1,1) - M2ID(a,0,1)*M2ID(a,1,0);
}

__attribute__((const))
__attribute__((cold))
float M3Det (M3 a)
{
	M2	a0 = {
		.m = {
			M3ID(a,1,1), M3ID(a,1,2),
			M3ID(a,2,1), M3ID(a,2,2)}
		},
		a1 = {
		.m = {
			M3ID(a,1,0), M3ID(a,1,2),
			M3ID(a,2,0), M3ID(a,2,2)}
		},
		a2 = {
		.m = {
			M3ID(a,1,0), M3ID(a,1,1),
			M3ID(a,2,0), M3ID(a,2,1)}
		};
	
	return a.m[0]*M2Det(a0) - a.m[1]*M2Det(a1) + a.m[2]*M2Det(a2);
}

__attribute__((const))
__attribute__((cold))
float M4Det (M4 a)
{
	M3	a0 = {
		.m = {
			M4ID(a,1,1), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,2,1), M4ID(a,2,2), M4ID(a,2,3),
			M4ID(a,3,1), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a1 = {
		.m = {
			M4ID(a,1,0), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,2,0), M4ID(a,2,2), M4ID(a,2,3),
			M4ID(a,3,0), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a2 = {
		.m = {
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,3),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,3),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,3)}
		},
		a3 = {
		.m = {
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,2),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,2),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,2)}
		};
	
	return a.m[0]*M3Det(a0) - a.m[1]*M3Det(a1) + a.m[2]*M3Det(a2) - a.m[3]*M3Det(a3);
}

__attribute__((const))
__attribute__((cold))
M2 M2Inv (M2 a)
{
	M2 ret = {
		.m = {
			 M2ID(a,1,1), -M2ID(a,0,1),
			-M2ID(a,1,0),  M2ID(a,0,0)}
		};
	
	return M2MulF(ret, 1.0f/M2Det(a));
}

__attribute__((const))
__attribute__((cold))
M3 M3Inv (M3 a)
{
	M2	a00 = {
		.m = {
			M3ID(a,1,1), M3ID(a,1,2),
			M3ID(a,2,1), M3ID(a,2,2)}
		},
		a01 = {
		.m = {
			M3ID(a,1,0), M3ID(a,1,2),
			M3ID(a,2,0), M3ID(a,2,2)}
		},
		a02 = {
		.m = {
			M3ID(a,1,0), M3ID(a,1,1),
			M3ID(a,2,0), M3ID(a,2,1)}
		};
	
	M2	a10 = {
		.m = {
			M3ID(a,0,1), M3ID(a,0,2),
			M3ID(a,2,1), M3ID(a,2,2)}
		},
		a11 = {
		.m = {
			M3ID(a,0,0), M3ID(a,0,2),
			M3ID(a,2,0), M3ID(a,2,2)}
		},
		a12 = {
		.m = {
			M3ID(a,0,0), M3ID(a,0,1),
			M3ID(a,2,0), M3ID(a,2,1)}
		};
		
	M2	a20 = {
		.m = {
			M3ID(a,0,1), M3ID(a,0,2),
			M3ID(a,1,1), M3ID(a,1,2)}
		},
		a21 = {
		.m = {
			M3ID(a,0,0), M3ID(a,0,2),
			M3ID(a,1,0), M3ID(a,1,2)}
		},
		a22 = {
		.m = {
			M3ID(a,0,0), M3ID(a,0,1),
			M3ID(a,1,0), M3ID(a,1,1)}
		};
	
	M3 ret = {
		.m = {
			M2Det(a00), M2Det(a10), M2Det(a20),
			M2Det(a01), M2Det(a11), M2Det(a21),
			M2Det(a02), M2Det(a12), M2Det(a22)}
		};
	
	return M3MulF(ret, 1.0f/(M3ID(ret,0,0)*M3ID(a,0,0) - M3ID(ret,1,0)*M3ID(a,0,1) + M3ID(ret,2,0)*M3ID(a,0,2)));
}

__attribute__((const))
__attribute__((cold))
M4 M4Inv (M4 a)
{
	M3	a00 = {
		.m = {
			M4ID(a,1,1), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,2,1), M4ID(a,2,2), M4ID(a,2,3),
			M4ID(a,3,1), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a01 = {
		.m = {
			M4ID(a,1,0), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,2,0), M4ID(a,2,2), M4ID(a,2,3),
			M4ID(a,3,0), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a02 = {
		.m = {
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,3),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,3),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,3)}
		},
		a03 = {
		.m = {
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,2),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,2),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,2)}
		};
	
	M3	a10 = {
		.m = {
			M4ID(a,0,1), M4ID(a,0,2), M4ID(a,0,3),
			M4ID(a,2,1), M4ID(a,2,2), M4ID(a,2,3),
			M4ID(a,3,1), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a11 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,2), M4ID(a,0,3),
			M4ID(a,2,0), M4ID(a,2,2), M4ID(a,2,3),
			M4ID(a,3,0), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a12 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,1), M4ID(a,0,3),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,3),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,3)}
		},
		a13 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,1), M4ID(a,0,2),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,2),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,2)}
		};

	M3	a20 = {
		.m = {
			M4ID(a,0,1), M4ID(a,0,2), M4ID(a,0,3),
			M4ID(a,1,1), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,3,1), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a21 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,2), M4ID(a,0,3),
			M4ID(a,1,0), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,3,0), M4ID(a,3,2), M4ID(a,3,3)}
		},
		a22 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,1), M4ID(a,0,3),
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,3),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,3)}
		},
		a23 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,1), M4ID(a,0,2),
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,2),
			M4ID(a,3,0), M4ID(a,3,1), M4ID(a,3,2)}
		};
	
	M3	a30 = {
		.m = {
			M4ID(a,0,1), M4ID(a,0,2), M4ID(a,0,3),
			M4ID(a,1,1), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,2,1), M4ID(a,2,2), M4ID(a,2,3)}
		},
		a31 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,2), M4ID(a,0,3),
			M4ID(a,1,0), M4ID(a,1,2), M4ID(a,1,3),
			M4ID(a,2,0), M4ID(a,2,2), M4ID(a,2,3)}
		},
		a32 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,1), M4ID(a,0,3),
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,3),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,3)}
		},
		a33 = {
		.m = {
			M4ID(a,0,0), M4ID(a,0,1), M4ID(a,0,2),
			M4ID(a,1,0), M4ID(a,1,1), M4ID(a,1,2),
			M4ID(a,2,0), M4ID(a,2,1), M4ID(a,2,2)}
		};
	
	M4 ret = {
		.m = {
			M3Det(a00), M3Det(a10), M3Det(a20), M3Det(a30),
			M3Det(a01), M3Det(a11), M3Det(a21), M3Det(a31),
			M3Det(a02), M3Det(a12), M3Det(a22), M3Det(a32),
			M3Det(a03), M3Det(a13), M3Det(a23), M3Det(a33)}
		};
	
	return M4MulF(ret, 1.0f/(M4ID(ret,0,0)*M3ID(a,0,0) - M4ID(ret,1,0)*M3ID(a,0,1) + M4ID(ret,2,0)*M3ID(a,0,2) - M4ID(ret,3,0)*M3ID(a,0,3)));
}


__attribute__((const))
inline M3 M3StackRotation (float a, M3 b)
{
	float c = cosf(a), s = sinf(a);
	M3 ret = b;
	
	for (int i = 0; i < 3; i++)
	{
		M3ID(ret,0,i) = c*M3ID(b,0,i) + s*M3ID(b,1,i);
		M3ID(ret,1,i) = c*M3ID(b,1,i) - s*M3ID(b,0,i);
	}
	
	return ret;
}

__attribute__((const))
inline M3 M3Rotate (float a)
{
	float c = cosf(a), s = sinf(a);
	
	M3 ret = {
		.m = {
			 c,-s, 0,
			 s, c, 0,
			 0, 0, 1}
		};
	
	return ret;
}

__attribute__((const))
inline M3 M3StackScale(V2 a, M3 b)
{
	M3 ret = b;
	
	for(int r = 0; r < 2; r++)
		for(int c = 0; c < 3; c++)
			M3ID(ret,r,c) *= a.v[r];
	
	return ret;
}

__attribute__((const))
inline M3 M3Scale (V2 a)
{
	M3 ret = {
		.m = {
			a.x, 0.0, 0.0,
			0.0, a.y, 0.0,
			0.0, 0.0, 1.0}
		};
	
	return ret;
}

__attribute__((const))
inline M3 M3StackTranslate (V2 a, M3 b)
{
	M3 ret = b;
	
	M3ID(ret,0,2) += a.x;
	M3ID(ret,1,2) += a.y;
	
	return ret;
}

__attribute__((const))
inline M4 M4StackRotateX (float a, M4 b)
{
	float c = cosf(a), s = sinf(a);
	M4 ret = b;
	
	for (int i = 0; i < 4; i++)
	{
		M4ID(ret,1,i) = c*M4ID(b,1,i) + s*M4ID(b,2,i);
		M4ID(ret,2,i) = c*M4ID(b,2,i) - s*M4ID(b,1,i);
	}
	
	return ret;
}

__attribute__((const))
inline M4 M4StackRotateY (float a, M4 b)
{
	float c = cosf(a), s = sinf(a);
	M4 ret = b;
	
	for (int i = 0; i < 4; i++)
	{
		M4ID(ret,0,i) = c*M4ID(b,0,i) - s*M4ID(b,2,i);
		M4ID(ret,2,i) = c*M4ID(b,2,i) + s*M4ID(b,0,i);
	}
	
	return ret;
}

__attribute__((const))
inline M4 M4StackRotateZ (float a, M4 b)
{
	float c = cosf(a), s = sinf(a);
	M4 ret = b;
	
	for (int i = 0; i < 4; i++)
	{
		M4ID(ret,0,i) = c*M4ID(b,0,i) + s*M4ID(b,1,i);
		M4ID(ret,1,i) = c*M4ID(b,1,i) - s*M4ID(b,0,i);
	}
	
	return ret;
}

__attribute__((const))
inline M4 M4Rotate (float a, int i, int j)
{
	M4 ret = {
		.m = {
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			0,0,0,1}
		};
	
	float c = cosf(a), s = sinf(a);
	
	M4ID(ret,i,i) =  c; M4ID(ret,i,j) =  s;
	M4ID(ret,j,i) = -s; M4ID(ret,j,j) =  c;
	
	return ret;
}

__attribute__((const))
inline M4 M4RotateX (float a)
{
	return M4Rotate(a,1,2);
}

__attribute__((const))
inline M4 M4RotateY (float a)
{
	return M4Rotate(a,2,0);
}

__attribute__((const))
inline M4 M4RotateZ (float a)
{
	return M4Rotate(a,0,1);
}

__attribute__((const))
inline M4 M4StackTranslate (V3 a, M4 b)
{
	M4 ret = b;
	
	for (int i = 0; i < 3; i++)
		M4ID(ret,i,0) += a.v[i];
	
	return ret;
}

__attribute__((const))
inline M4 M4Translate (V3 a)
{
	M4 ret = {
		.m = {
			1.0, 0.0, 0.0, a.x,
			0.0, 1.0, 0.0, a.y,
			0.0, 0.0, 1.0, a.z,
			0.0, 0.0, 0.0, 1.0}
		};
	
	return ret;
}

__attribute__((const))
inline M4 M4StackScale (V3 a, M4 b)
{
	M4 ret = b;
	
	for (int r = 0; r < 3; r++)
	{
		float s = a.v[r];
		for (int c = 0; c < 4; c++)
			M4ID(ret,r,c) *= s;
	}
	
	return ret;
}

__attribute__((const))
inline M4 M4Scale (V3 a)
{
	M4 ret = {
		.m = {
			a.x, 0.0, 0.0, 0.0,
			0.0, a.y, 0.0, 0.0,
			0.0, 0.0, a.z, 0.0,
			0.0, 0.0, 0.0, 1.0}
		};
	
	return ret;
}

__attribute__((const))
inline M4 M4Perspective (float f)
{
	M4 ret = {
		.m = {
			1.0, 0.0, 0.0, 0.0,
			0.0, 1.0, 0.0, 0.0,
			0.0, 0.0, 1.0, 0.0,
			0.0, 0.0, 1/f, 0.0}
		};
	
	return ret;
}

//	Grelk  additions

#include <stdio.h>

__attribute__((const))
void M3Print(M3 a)
{
	for(int i=0; i<9; i++)
		printf(i%3==2 ? "%5.2f\n" : "%5.2f, ", a.m[i]);
	printf("\n");
}

__attribute__((const))
inline V2 V2Lerp(V2 a, V2 b, float c)
{
	return V2Add(V2Scale(a,1-c), V2Scale(b,c));
}

__attribute__((const))
inline V2 V2CirclePos(V2 c, float r, float a)
{
	V2 tmp =
	{	.x = sinf(a)
	,	.y = cosf(a)
	};
	return V2Add(c, V2Scale(tmp, r));
}

__attribute__((const))
inline V2 V2Polar(float a, float r)
{
	V2 ret =
	{	.x = r*cosf(a)
	,	.y = r*sinf(a)
	};
	return ret;
}

__attribute__((const))
inline V2 V2Rotate(V2 v, float a)
{
	float c = cosf(a);
	float s = sinf(a);
	V2 ret =
	{	.x = v.x*c - v.y*s
	,	.y = v.x*s + v.y*c
	};
	
	return ret;
}

__attribute__((const))
inline V2 V2RotatePreComp(V2 v, float c, float s)
{
	V2 ret =
	{	.x = v.x*c - v.y*s
	,	.y = v.x*s + v.y*c
	};
	
	return ret;
}

__attribute__((const))
inline V2 V2YClockPolar(float a, float r)
{
	V2 ret =
	{	.x = r*sinf(a)
	,	.y = r*cosf(a)
	};
	return ret;
}

__attribute__((const))
inline float V2MagSquared(V2 a)
{
	return V2Dot(a, a);
}

__attribute__((const))
inline V2 V2Min(V2 a, V2 b)
{
	V2 ret =
	{	.x = a.x < b.x ? a.x : b.x
	,	.y = a.y < b.y ? a.y : b.y
	};
	return ret;
}

__attribute__((const))
inline float V2DistanceSquared (V2 a, V2 b)
{
	return V2MagSquared(V2Sub(a, b));
}
