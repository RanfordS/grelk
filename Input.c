#include "Core.h"
#include "Input.h"
#include "Draw.h"

//! Current time
Uint32 tick = 0;
//!	High-precision delta time value.
double dt;
double physdt;

/**
The Keystate Map contains the state of all the keys, indexed by scancode.
The high bit indicates whether the key is down while.
The low bit indicates whether the key has changes state this frame.
*/

Uint8 Actions[a_num];

M3 aspectmat = {.m={
	1,0,0,
	0,1,0,
	0,0,1}};

bool debugreq = false;
bool fixdt = false;

///	Parses sdl events, packs keystates and updates delta time.
bool DoEvents(void)
{
	bool run = true;
	
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		//SDL_Scancode sc;
		switch(event.type)
		{
			case SDL_QUIT:
				run = false;
				break;
				
			case SDL_WINDOWEVENT:
				switch(event.window.event)
				{
					
					case SDL_WINDOWEVENT_SIZE_CHANGED:
						glViewport(0, 0, event.window.data1, event.window.data2);
						glLoadIdentity();
						{
							float smallest = event.window.data1 < event.window.data2 ? event.window.data1 : event.window.data2;
							GLfloat x = event.window.data1/smallest;
							GLfloat y = event.window.data2/smallest;
							//glOrtho(-x, x, -y, y, -1, 3);
							glOrtho(-1, 1, -1, 1, -1, 3);
							printf("Window size changed, rel: %.2f, %.2f\n", x, y);
							M3ID(aspectmat, 0, 0) = 1/x;
							M3ID(aspectmat, 1, 1) = 1/y;
						}
						break;
				}
				break;
				
			case SDL_KEYDOWN:
				if(event.key.repeat==0)
				{
					if(event.key.keysym.sym == SDLK_d)
						debugreq = true;
					else if(debugreq) switch(event.key.keysym.sym)
					{
						case SDLK_h:
							printf(
								"[#]:\tDebug request help\n"
								"\ta : Action input states\n"
								"\tc : Camera target position\n"
								"\tt : Toggle fixed dt stepping, use the console to advance time steps\n"
								"\th : Help\n");
							break;
						case SDLK_c:
							printf("[#]:\tCamera.tar (%4.2f, %4.2f, %4.2f)\n", Camera.tar.x, Camera.tar.y, Camera.targetScale);
							break;
						case SDLK_a:
							for(size_t a = 0; a < a_num; a++)
								printf("[#]:\t %8s : d%is%i\n", ActionLabel[a], Actions[a]&km_delta, Actions[a]&km_state);
							printf("\n");
							break;
						case SDLK_t:
							fixdt ^= true;
							printf("[#]:\tFixed dt toggled\n");
							break;
					}
				}
				break;
				
			case SDL_KEYUP:
				if(event.key.keysym.sym == SDLK_d)
					debugreq = false;
				
			default:
				break;
		}
	}
	
	//	action mapping
	const Uint8* state = SDL_GetKeyboardState(NULL);	//	gets SDL's keystate map
	
	size_t b = 0;
	for(size_t a = 0; a < a_num; a++)
	{
		Uint8 new = 0;
		
		for(size_t m = Mappings; m; b++)
		{
			m--;
			SDL_Scancode key = Bindings[b].key;
			SDL_Scancode mod = Bindings[b].mod;
			
			if(key)
			{
				Uint8 sub = state[key];
				
				if(mod)
					sub &= state[mod];
				
				new |= sub;
			}
		}
		//	update key delta
		Uint8 old = Actions[a] & km_state;
		Actions[a] = new | ((old ^ new) << 1);
	}
	
	if(fixdt)
	{
		char i = getchar();
		switch(i)
		{
			case 'z':
				Actions[a_jump] = ks_pressed;
				break;
			case 't':
				fixdt = false;
				break;
		}
	}
	//*/
	//	calculate delta time
	Uint32 new = SDL_GetPerformanceCounter();
	dt = ((double)(new - tick))/SDL_GetPerformanceFrequency();
	tick = new;
	if(fixdt)
		dt = 1/60.f;
	physdt = dt;
	return run;
}
