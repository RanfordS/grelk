#version 330
in vec2 ST;
uniform sampler2D tex;
out vec4 gl_FragColor;	//	specific name not required

void main(void)
{
	gl_FragColor = texture(tex, ST);
}
