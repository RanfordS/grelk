#pragma once
#include "Core.h"
//~@>	Draw.h   <@~//

#define NumPrimeTextures 64
//#define Mat4ID(M,r,c) M[4*r + c]
//#define Mat3ID(M,r,c) M[3*r + c]

typedef struct
{	V2 pos;
	V2 tar;
	float currentScale;
	float targetScale;
	float actualScale;
	float currentShake;
	float actualShake;
	float lead;
	float lag;
} cam;
extern cam Camera;

typedef struct
{	union
	{	struct
		{	GLfloat x;
			GLfloat y;
			GLfloat s;
			GLfloat t;
		};
	};
} rglPoint;

typedef struct
{	rglPoint A;
	rglPoint B;
	rglPoint C;
} rglTriangle;

typedef struct
{	rglPoint A;
	rglPoint B;
	rglPoint C;
	rglPoint D;
} rglQuadrangle;

typedef struct
{	GLuint VBO;
	GLuint IBO;
	GLsizei count;
} Chunk;

#define ChunkSizeX 16
#define ChunkSizeY 8

extern bool GraphicsInit(void);
extern bool DoEvents(void);
extern void Draw(void);
extern void GraphicsQuit(void);
//extern void Mat4Print(GLfloat* A);
//extern void Mat3Print(GLfloat* A);

extern float camtarx;
extern float camtary;
extern float camtarz;
