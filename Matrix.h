/*~@> Matrix.h <@~*/
#pragma once
#include <math.h>

//	V2 generics

typedef struct V2
{
	union
	{	float v[2];
		struct
		{	float x, y;
		};
		struct
		{	float s, t;
		};
	};
} V2;

extern V2 V2Add (V2 a, V2 b);
extern V2 V2Sub (V2 a, V2 b);
extern V2 V2Mul (V2 a, V2 b);
extern float V2Sum (V2 a);
extern V2 V2Scale (V2 a, float b);
extern float V2Dot (V2 a, V2 b);
extern float V2Magnitude (V2 a);
extern V2 V2Normalise (V2 a);

typedef struct M2
{
	float m[2*2];
} M2;

#define M2ID(M2,r,c) M2.m[2*(r) + (c)]

extern M2 M2MulF (M2 a, float b);
extern V2 M2MulV2 (M2 a, V2 b);
extern M2 M2Mul (M2 a, M2 b);
extern M2 M2Transpose (M2 a);

//	V3 generics

typedef struct V3
{
	union
	{	float v[3];
		struct
		{	float x, y, z;
		};
		struct
		{	float s, t, w;
		};
		struct
		{	float r, g, b;
		};
	};
} V3;

extern V3 V3Add (V3 a, V3 b);
extern V3 V3Sub (V3 a, V3 b);
extern V3 V3Mul (V3 a, V3 b);
extern float V3Sum (V3 a);
extern V3 V3Scale (V3 a, float b);
extern float V3Dot (V3 a, V3 b);
extern float V3Magnitude (V3 a);
extern V3 V3Normalise (V3 a);

typedef struct M3
{
	float m[3*3];
} M3;

#define M3ID(M3,r,c) M3.m[3*(r) + (c)]

extern M3 M3MulF (M3 a, float b);
extern V3 M3MulV3 (M3 a, V3 b);
extern M3 M3Mul (M3 a, M3 b);
extern M3 M3Transpose (M3 a);

//	V4 generics

typedef struct V4
{
	union
	{	float v[4];
		struct
		{	float x, y, z, w;
		};
		struct
		{	float r, g, b, a;
		};
	};
} V4;

extern V4 V4Add (V4 a, V4 b);
extern V4 V4Sub (V4 a, V4 b);
extern V4 V4Mul (V4 a, V4 b);
extern float V4Sum (V4 a);
extern V4 V4Scale (V4 a, float b);
extern float V4Dot (V4 a, V4 b);
extern float V4Magnitude (V4 a);
extern V4 V4Normalise (V4 a);

typedef struct M4
{
	float m[4*4];
} M4;

#define M4ID(M4,r,c) M4.m[4*(r) + (c)]

extern M4 M4MulF (M4 a, float b);
extern V4 M4MulV4 (M4 a, V4 b);
extern M4 M4Mul (M4 a, M4 b);
extern M4 M4Transpose (M4 a);

//	Non-genercis
extern V3 V3Cross (V3 a, V3 b);
extern V3 V2HomogeneousV3 (V2 a);
extern V4 V3HomogeneousV4 (V3 a);
extern float M2Det (M2 a);
extern float M3Det (M3 a);
extern float M4Det (M4 a);
extern M2 M2Inv (M2 a);
extern M3 M3Inv (M3 a);
extern M4 M4Inv (M4 a);
extern M3 M3Scale (V2 a);
extern M3 M3StackTranslate (V2 a, M3 b);
extern M3 M3StackRotation (float a, M3 b);
extern M3 M3Rotate (float a);

//	Grelk Additions

extern void M3Print(M3 a);
extern V2 V2Lerp(V2 a, V2 b, float c);
extern V2 V2CirclePos(V2 c, float r, float a);
extern V2 V2Rotate(V2 v, float a);
extern V2 V2RotatePreComp(V2 v, float c, float s);
extern V2 V2Polar(float a, float r);
extern float V2MagSquared(V2 a);
extern float V2DistanceSquared (V2 a, V2 b);
