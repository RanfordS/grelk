#include "Core.h"
#include "World.h"



//!	Lookup table for tile segment data, consisting of the tiles segment offset and the number of segments of the tile.
const tt_slup TileTypeSegmentLookup[tt_num] =
{	[tt_air   ] = {.off =  0, .num = 0}

,	[tt_flat  ] = {.off =  0, .num = 1}

,	[tt_sharp ] = {.off =  1, .num = 1}

,	[tt_sconl ] = {.off =  2, .num = 2}
,	[tt_sc    ] = {.off =  4, .num = 1}
,	[tt_sconr ] = {.off =  5, .num = 2}

,	[tt_lconl ] = {.off =  7, .num = 2}
,	[tt_lcsegl] = {.off =  9, .num = 1}
,	[tt_lc    ] = {.off = 10, .num = 1}
,	[tt_lcsegr] = {.off = 11, .num = 1}
,	[tt_lconr ] = {.off = 12, .num = 2}

,	[tt_sqew  ] = {.off = 14, .num = 1}
};

//!	Array containing all line segments for all tiles.
const TilePoint TileSegments[] =
{	//	tt_flat
	{	.type = teqt_linear
	,	.position = {.x=0, .y=-SQRT3/12}
	,	.halflength = 0.25
	,	.linear =
		{	.direction = {.x=1, .y=0}
		,	.angle = 0
		}
	}	//!<	tt_flat segment 1/1.
,	//	tt_sharp
	{	.type = teqt_radial
	,	.position = {.x=0, .y=-SQRT3/6}
	,	.halflength = M_PI*R3O2/9
	,	.radial =
		{	.midAngle = 0
		,	.radius = SQRT3/6
		,	.inside = false
		}
	}	//!<	tt_sharp segment 1/1.
,	//	tt_sconl
	{	.type = teqt_linear
	,	.position = {.x=-0.125, .y=-SQRT3/12}
	,	.halflength = 0.125
	,	.linear =
		{	.direction = {.x=1, .y=0}
		,	.angle = 0
		}
	}	//!<	tt_sconl segment 1/2.
,
	{	.type = teqt_radial
	,	.position = {.x=0, .y=-SQRT3/3}
	,	.halflength = M_PI*SQRT3/48
	,	.radial =
		{	.midAngle = M_PI/12
		,	.radius = SQRT3/4
		,	.inside = false
		}
	}	//!<	tt_sconl segment 2/2.
,	//	tt_sc
	{	.type = teqt_radial
	,	.position = {.x=0, .y=-SQRT3/3}
	,	.halflength = M_PI*SQRT3/24
	,	.radial =
		{	.midAngle = 0
		,	.radius = SQRT3/4
		,	.inside = false
		}
	}	//!<	tt_sc segment 1/1.
	,
	{	.type = teqt_radial
	,	.position = {.x=0, .y=-SQRT3/3}
	,	.halflength = M_PI*SQRT3/48
	,	.radial =
		{	.midAngle = -M_PI/12
		,	.radius = SQRT3/4
		,	.inside = false
		}
	}	//!<	tt_sconr segment 1/2.
,	//	tt_sconl
	{	.type = teqt_linear
	,	.position = {.x=0.125, .y=-SQRT3/12}
	,	.halflength = 0.125
	,	.linear =
		{	.direction = {.x=1, .y=0}
		,	.angle = 0
		}
	}	//!<	tt_sconr segment 2/2.
};

const float TileAngle[to_m] =
{	[to_b ]	=  0
,	[to_bi] =  0
,	[to_l ] = +2*M_PI/3
,	[to_li] = +2*M_PI/3
,	[to_r ] = -2*M_PI/3
,	[to_ri] = -2*M_PI/3
};

const bool TileInvert[to_m] =
{	[to_b ]	= false
,	[to_bi] = true
,	[to_l ]	= false
,	[to_li] = true
,	[to_r ]	= false
,	[to_ri] = true
};

V2 getVisualPos(V2 pos, size_t tr, size_t tc, float* theta, bool makeGlobal, bool tileLock, bool dbg)
{
//	if(dbg) printf("gVP((%5.3f,%5.3f), %3zi, %3zi, ->, %i, %i)\n", pos.x, pos.y, tc, tr, makeGlobal, tileLock);
	V2 vis = pos;
	//	is the tile the right way up
	bool upness = (tr^tc)&1;
	if(tileLock)
	{	//	is the tile the right way up
		
		float tileCos;
		float tileSin;
		
		TileOrient orient = CurrentLevel.tile[16*tr + tc] & to_m;
		bool inv = false;
		
		switch(orient)
		{
			default:
				printf("[!]:\tAttempted to use invalid orientation\n\tPos[%zi,%zi](%5.2f,%5.2f)\n\tOrient = %i\n", tr, tc, pos.x, pos.y, orient);
		
			case to_bi:
				inv = true;
			case to_b:
				tileCos = 1.0f;
				tileSin = 0.0f;
				*theta = 0;
				break;
			
			case to_li:
				inv = true;
			case to_l:
				tileCos = -0.5f;
				tileSin = -R3O2;
			 	*theta =  2*M_PI/3;
				break;
			
			case to_ri:
				inv = true;
			case to_r:
				tileCos = -0.5f;
				tileSin =  R3O2;
				*theta  = -2*M_PI/3;
				break;
		}
		
		if(inv)
			vis.x *= -1;
		
		uint16_t tile = CurrentLevel.tile[16*tr + tc] >> TT_Offset;
		uint16_t segid = TileTypeSegmentLookup[tile].off + (uint16_t)pos.y;
		TilePoint seg = TileSegments[segid];
		
		switch(seg.type)
		{
			case teqt_linear:
				vis = V2Scale(seg.linear.direction, vis.x);
				*theta += seg.linear.angle;
				break;
			
			case teqt_radial:
				*theta += seg.radial.midAngle + vis.x/seg.radial.radius;
				vis = V2Polar(M_PI/2 - (seg.radial.midAngle + vis.x/seg.radial.radius), seg.radial.radius);
				if(seg.radial.inside)
					*theta += M_PI;
				break;
			
			default:
				printf("[!]:\tAttempted to use invalid tile equation\n\tPos[%zi,%zi](%5.2f,%5.2f)\n\ttype = %i", tr, tc, pos.x, pos.y, seg.type);
				break;
		}
		vis = V2Add(vis, seg.position);
		
		vis = V2RotatePreComp(vis, tileCos, tileSin);
		
		if(!upness)
			vis = V2RotatePreComp(vis, -1, 0);
		
		//	correct angle for upness and inversion
		if(!(upness ^ inv))
		{
			*theta += M_PI;
		}
	}
	if(makeGlobal)
	{	//	make global
		vis.x += (tc+1)*0.5;
		vis.y += R3O2*(tr + (upness ? 2.0/3 : 1.0/3));
	}
	return vis;
}
