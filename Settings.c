#include "Draw.h"

#define SettingsFile "settings.ini"

int WindowX = SDL_WINDOWPOS_UNDEFINED;
int WindowY = SDL_WINDOWPOS_UNDEFINED;
int WindowW = 800;
int WindowH = 600;
Uint32 WindowF = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;

bool WriteSettings(void)
{
	FILE* setsf = NULL;
	setsf = fopen(SettingsFile, "w");
	
	if(!setsf)
	{	printf("[!]:\tCould not open settings file for writing.\n");
		return false;
	}
	
	fprintf(setsf ,"# Manual edit with great caution!\n# Delete this file to return to the default settings\n# <setting> = <integer value>\n");
	
	if(WindowX != SDL_WINDOWPOS_UNDEFINED)
		fprintf(setsf, "WindowX = %i\n", WindowX);
	
	if(WindowY != SDL_WINDOWPOS_UNDEFINED)
		fprintf(setsf, "WindowY = %i\n", WindowY);
	
	fprintf(setsf, "WindowW = %i\n", WindowW);
	fprintf(setsf, "WindowH = %i\n", WindowH);
	
	if(WindowF & SDL_WINDOW_RESIZABLE)
		fprintf(setsf, "WindowResizeable = 1\n");
	
	if(WindowF & SDL_WINDOW_FULLSCREEN)
		fprintf(setsf, "WindowFullscreen = 1\n");
	
	if(WindowF & SDL_WINDOW_FULLSCREEN_DESKTOP)
		fprintf(setsf, "WindowFullscreen = 2\n");
	
	if(WindowF & SDL_WINDOW_BORDERLESS)
		fprintf(setsf, "WindowBorderless = 1\n");
	
	return true;
}

bool ReadSettings(char* path)
{	printf("ReadSettings\n");
	FILE* setsf = NULL;
	bool opened = true;
	
	if(path)
	{	printf("[ ]:\tAttepting %s\n", path);
		setsf = fopen(path, "r");
	}
	
	if(!setsf)
	{
		if(path)
		{	printf("[?]:\tCould not open %s, attempting default file\n", SettingsFile);
			opened = false;
		}
		setsf = fopen(SettingsFile, "r");
	}
	
	if(!setsf)
	{	printf("[?]:\tCould not open %s, no changes were made to the settings\n", SettingsFile);
		opened = false;
		WriteSettings();
	}
	else
	{
		int n;
		do
		{
			char option[16];
			int setting;
			
			n = fscanf(setsf, "%s = %i\n", option, &setting);
			if(n==2 && option[0]!='#')
			{
					 if(strstr(option, "WindowW"))
					WindowW = setting;
				else if(strstr(option, "WindowH"))
					WindowH = setting;
				else if(strstr(option, "WindowX"))
					WindowX = setting == -1 ? SDL_WINDOWPOS_UNDEFINED : setting;
				else if(strstr(option, "WindowY"))
					WindowY = setting == -1 ? SDL_WINDOWPOS_UNDEFINED : setting;
				else if(strstr(option, "WindowResizeable"))
					WindowF |= setting == 1 ? SDL_WINDOW_RESIZABLE : 0;
				else if(strstr(option, "WindowFullscreen"))
					WindowF |= setting == 1 ? SDL_WINDOW_FULLSCREEN : setting == 2 ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0;
				else if(strstr(option, "WindowBorderless"))
					WindowF |= setting == 1 ? SDL_WINDOW_BORDERLESS : 0;
			}
		}
		while(n!=-1);
		
		fclose(setsf);
	}
	return opened;
}
