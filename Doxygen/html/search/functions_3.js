var searchData=
[
  ['generalphysicspass',['GeneralPhysicsPass',['../_entity_8c.html#aa2c64671c1b363bbe6c2fed92d8b6724',1,'Entity.c']]],
  ['getvisualpos',['getVisualPos',['../_world_8h.html#ad0e402efe989d42fd5cf3cf7fc6e3c3e',1,'getVisualPos(V2 pos, size_t tr, size_t tc, float *theta, bool makeGlobal, bool tileLock, bool dbg):&#160;WorldData.c'],['../_world_data_8c.html#ad0e402efe989d42fd5cf3cf7fc6e3c3e',1,'getVisualPos(V2 pos, size_t tr, size_t tc, float *theta, bool makeGlobal, bool tileLock, bool dbg):&#160;WorldData.c']]],
  ['graphicsinit',['GraphicsInit',['../_draw_8c.html#a3fa014aa5e033ea0bc2fde21467c60c1',1,'GraphicsInit(void):&#160;Draw.c'],['../_draw_8h.html#a3fa014aa5e033ea0bc2fde21467c60c1',1,'GraphicsInit(void):&#160;Draw.c']]],
  ['graphicsquit',['GraphicsQuit',['../_draw_8c.html#a0735234ed0503e1e297bc31f605bb462',1,'GraphicsQuit():&#160;Draw.c'],['../_draw_8h.html#aad575506dc31eef5da41350b6ba510fc',1,'GraphicsQuit(void):&#160;Draw.c']]]
];
