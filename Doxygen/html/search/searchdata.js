var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyz",
  1: "bcelmrstv",
  2: "abcdeimpsw",
  3: "_cdgilmprsvw",
  4: "abcdefghiklmnoprstuvwxyz",
  5: "mv",
  6: "aegkmstw",
  7: "aegkmstw",
  8: "abcdegmnprstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

