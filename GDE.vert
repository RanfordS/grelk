#version 330
layout (location = 0) in vec3 inPos;
layout (location = 1) in vec2 inST;
out vec2 ST;

void main(void)
{
	ST = inST;
	gl_Position = vec4(inPos, 1.0);
	
}
