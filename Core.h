#pragma once

//~@>	Core.h   <@~//
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "aMatrix.h"

typedef enum
{	gs_init
,	gs_mainmenu
,	gs_play
} GameState;

typedef enum
{	mmss_start
,	mmss_levelselect
} MainMenuSubState;

extern double time;

//!	Magic number: Square root of 3.
#define SQRT3 1.73205077648162841796875f
//!	Magic number: Square root of 3, over 2.
#define R3O2  0.866025388240814208984375f

extern float r3o2;	//!	sqrt(3)/2

#define mMax(a,b) ((a)>(b)?(a):(b))
#define mMin(a,b) ((a)<(b)?(a):(b))

#define Upness(x,y) (((x)^(y))&1)

#define doif if
#define elif else if

//~@>	Input.h   <@~//
extern double dt;
extern double physdt;
