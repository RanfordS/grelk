#include "Core.h"
#include "Input.h"

#define BindingsFile "binds.ini"

Bind Bindings[Mappings * a_num];

char ActionLabel[a_num][8] =
{	"left"
,	"right"
,	"up"
,	"down"
,	"jump"
,	"attack"
};

//!	Writes the keybindings to the binds file, returns false if the bindings file cannot be opened for writting
bool WriteBinds(void)
{
	FILE* bindsf = fopen(BindingsFile, "w");
	
	if(!bindsf)
	{	printf("[!]:\tFailed to write binds to %s\n", BindingsFile);
		return false;
	}
	
	fprintf(bindsf, "# Manual edit at ones own peril!\n# Delete this file to return to default bindings\n# <action> : <mapping number> = <key scancode> & <modfier scancode>\n");
	
	
	//	all actions
	for(Action a = 0; a < a_num; a++)
	{
		for(unsigned char i = 0; i < Mappings; i++)
		{
			Bind b = Bindings[Mappings * a + i];
			if(b.key)
			{
				fprintf(bindsf, "%s : %i = %i", ActionLabel[a], i, b.key);
				
				if(b.mod)
					fprintf(bindsf, " & %i", b.mod);
				
				fprintf(bindsf, "\n");
			}
		}
	}
	
	fclose(bindsf);
	
	return true;
}

//!	Attempts to apply the bind settings the bindings file from the path.
//! If it can't open the file for reading, it will try the default bindings file.
//! If this also fails set the bindings to the default and attempt to write the defaults with WriteBinds().
//! Returns a bool indicating whether the requested file was read. Use NULL to read the binds from the default path.
bool ReadBinds(char* path)
{	printf("ReadBinds\n");
	FILE* bindsf = NULL;
	bool opened = true;
	
	if(path)
	{	printf("[ ]:\tAttempting %s\n", path);
		bindsf = fopen(path, "r");
	}
	
	if(!bindsf)
	{
		if(path)
		{	printf("[?]:\tCould not open %s, attempting default file\n", path);
			opened = false;
		}
		bindsf = fopen(BindingsFile, "r");
	}
	
	memset(Bindings, 0, sizeof(Bindings));
	
	if(!bindsf)
	{
		printf("[?]:\tCould not open %s, setting to factory binds and attempting to write defaults\n", BindingsFile);
		opened = false;
		//~@>	factory defaults binds
		Bindings[Mappings * a_left].key		= SDL_SCANCODE_LEFT;
		Bindings[Mappings * a_right].key	= SDL_SCANCODE_RIGHT;
		Bindings[Mappings * a_up].key		= SDL_SCANCODE_UP;
		Bindings[Mappings * a_down].key		= SDL_SCANCODE_DOWN;
		Bindings[Mappings * a_jump].key		= SDL_SCANCODE_Z;
		Bindings[Mappings * a_attack].key	= SDL_SCANCODE_X;
		WriteBinds();
	}
	else
	{
		printf("[ ]:\tReading settings\n");
		//~@> TODO: Test the read functionality, 'cos who knows if it'll work
		bool read = true;
		while(read)
		{
			size_t bufsize = 0;
			char* buf = NULL;
			int n = getline(&buf, &bufsize, bindsf);
			
			printf("Line: %s", buf);
			
			if(n <= 0)
				read = false;
			else if(buf[0] != '#')
			{
				char action[8];
				size_t id;
				SDL_Scancode key;
				SDL_Scancode mod;
				
				n = sscanf(buf, "%s : %zi = %u & %u\n", action, &id, &key, &mod);
				
				if(n > 2)
				{	//	find action id
					for(Action a = 0; a < a_num; a++)
						if(strstr(action, ActionLabel[a]))
							id += Mappings * a;
					
					printf("Interp: Id = %zi, key = %i, mod = %i\n", id, key, mod);
					
					if(id < Mappings * a_num)
					{
						Bindings[id].key = key;
						Bindings[id].mod = n==4 ? mod : 0;
					}
				}
			}
			
			free(buf);
		}
		
		printf("Done\n");
		
		fclose(bindsf);
	}
	
	return opened;
}
