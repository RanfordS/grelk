# C compiler flags
CCFLAGS	= -Wall -Wextra -march=native -Og -g
# Linker flags
LDFLAGS	= -Wall -Wextra -march=native -Og -g
# Libs to link with
LNKLIBS = -lGL -lGLU -lGLEW -lSDL2 -lSDL2_image -lm
# Objects files to make
OBJS	= Main.o Input.o Draw.o Binds.o Settings.o World.o Entity.o Matrix.o Player.o Perlin.o WorldData.o
# Where to put build files
OBJLOC	= obj/
# The final program name
OUTPUT	= Grelk


$(OUTPUT) : $(addprefix $(OBJLOC), $(OBJS))
	@echo -e '\e[1;36mLINKING\033[0m'
	gcc $(LDFLAGS) -o $(OUTPUT) $(addprefix $(OBJLOC), $(OBJS)) $(LNKLIBS)
	@echo -e '\e[1;36mDONE\033[0m'
	
-include $(addprefix $(OBJLOC), $(OBJS:.o=.d))

$(OBJLOC)%.o: %.c
#	-@echo 'DEPENDS' | cat - $(OBJLOC)$*.d
	@echo -e '\e[1;36mBUILDING\033[0m: $*.c'
	gcc -c $(CCFLAGS) $*.c -o $(OBJLOC)$*.o
	@cpp -MM $*.c | lua make-help.lua $(OBJLOC) > $(OBJLOC)$*.d

.PHONY : clean
clean :
	@echo -e '\e[1;36mCLEANING\033[0m'
	@rm -fr $(OUTPUT) $(OBJLOC)*
	@echo -e '\e[1;36mDONE\033[0m'

.PHONY : doc
doc :
	@echo -e '\e[1;36mDOCUMENTING\033[0m'
	@doxygen doxyfile
	@echo -e '\e[1;36mDONE\033[0m'
