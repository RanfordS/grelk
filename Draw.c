#include "Core.h"
#include "Draw.h"
#include "World.h"
#include "Input.h"
#include "Entity.h"
#include "Perlin.h"
//#define rGL_CONTEXTLIMIT //~@> Check if this is needed

//	http://lazyfoo.net/tutorials/SDL/51_SDL_and_modern_opengl/index.php
SDL_Window* Window = NULL;
SDL_GLContext Context = NULL;

int WindowWidth  = 800;
int WindowHeight = 600;

GLuint* LoadedTextures = NULL;
GLuint PrimeTextures[NumPrimeTextures];

/*
bool GraphicsInit(void)
{
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{	printf("[!]: SDL_Init failed\n");
		return false;
	}
	if(IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
	{	printf("[!]: IMG_Init failed\n");
		return false;
	}
	
	#ifdef rGL_CONTEXTLIMIT
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	#endif
	
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	
	Window = SDL_CreateWindow("Grelk: Swiftness", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowWidth, WindowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if(!Window) return false;
	Context = SDL_GL_CreateContext(Window);
	if(!Context) return false;
	
	glEnable(GL_BLEND);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	
	return true;
}
*/

typedef struct
{	GLuint program;
	GLint inPos;
	GLint inST;
	GLint texLoc;
	GLint matLoc;
	GLint stmatLoc;
} Shader;


Shader* CreateShader(void)
{
	GLint res;
	GLuint ProgramID = glCreateProgram();
	
	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertShaderCode[] = {
		"#version 130\n"
		"in vec2 inPos;\n"
		"in vec2 inST;\n"
		"uniform mat3 M;\n"
		"uniform mat3 stM;\n"
		"out vec2 ST;\n"
		"out vec4 pos;\n"// DEBUG
		"void main()\n"
		"{	ST = (stM * vec3(inST,1)).xy;\n"
		"	gl_Position = vec4(M * vec3(inPos, 1), 0).xywz;\n"
		"	pos = gl_Position;"// DEBUG
		"}"
	};
	glShaderSource(vertShader, 1, vertShaderCode, NULL);
	glCompileShader(vertShader);
	
	res = GL_TRUE;
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &res);
	if(res == GL_FALSE)	//	potential change that assumes that compilation is successful: if(__builtin_expect(res == GL_FALSE, 0))
	{	GLint ls = 0; glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &ls);
		GLchar* error = malloc(ls*sizeof(GLchar));
		glGetShaderInfoLog(vertShader, ls, &ls, error);
		printf("[!]:\tFailed to compile vertShader: %s\n", error);
		glDeleteShader(vertShader);
		glDeleteProgram(ProgramID);
		return NULL;
	}
	glAttachShader(ProgramID, vertShader);
	
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragShaderCode[] = {
		"#version 130\n"
		"in vec2 ST;\n"
		"in vec4 pos;\n"// DEBUG
		"uniform sampler2D image;\n"
		"void main()\n"
		"{	gl_FragColor = vec4(mod(pos.xyz, 0.2)/0.2, 1)*0.0 + vec4(mod(ST,0.125)/0.125,0,1)*0.0 + 1.0*texture(image, ST);\n"
		"}"
	};
	glShaderSource(fragShader, 1, fragShaderCode, NULL);
	glCompileShader(fragShader);
	res = GL_TRUE;
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &res);
	if(res == GL_FALSE)	//	if(__builtin_expect(res == GL_FALSE, 0))
	{	GLint ls = 0; glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &ls);
		GLchar* error = malloc(ls*sizeof(GLchar));
		glGetShaderInfoLog(fragShader, ls, &ls, error);
		printf("[!]:\tFailed to compile fragShader: %s\n", error);
		glDeleteShader(fragShader);
		glDeleteShader(vertShader);
		glDeleteProgram(ProgramID);
		return NULL;
	}
	glAttachShader(ProgramID, fragShader);
	
	glLinkProgram(ProgramID);
	glDeleteShader(fragShader);
	glDeleteShader(vertShader);
	res = GL_TRUE;
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &res);
	if(res == GL_FALSE)
	{	GLint ls = 0; glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &ls);
		GLchar* error = malloc(ls*sizeof(GLchar));
		glGetProgramInfoLog(ProgramID, ls, &ls, error);
		printf("[!]:\tFailed to link program: %s\n", error);
		glDeleteProgram(ProgramID);
		return NULL;
	}
	
	Shader* S = malloc(sizeof(Shader));
	S->program = ProgramID;
	S->inPos = glGetAttribLocation(ProgramID, "inPos");
	S->inST = glGetAttribLocation(ProgramID, "inST");
	S->texLoc = glGetUniformLocation(ProgramID, "image");
	S->matLoc = glGetUniformLocation(ProgramID, "M");
	S->stmatLoc = glGetUniformLocation(ProgramID, "stM");
	
	printf("s6: pid %i, inpos %i, inst %i, texloc %i, matloc %i\n", S->program, S->inPos, S->inST, S->texLoc, S->matLoc);
	
	return S;
}

Shader* MainShader = NULL;
GLuint TerrainTexture = 0;



//!	Loads the image as a GL texture and returns its texture id.
GLuint R_LoadImage(const char* path)
{
	printf("[ ]:\tLoading Image: \"%s\"\n", path);
	SDL_Surface* Surface = IMG_Load(path);
	GLuint ID;
	glGenTextures(1, &ID);
	glBindTexture(GL_TEXTURE_2D, ID);
	//	TODO: find out what the difference between format and internalFormat is and adjust accordingly
	int Mode = GL_RGBA; //Surface->format->BytesPerPixel == 4 ? GL_RGBA : GL_RGB;
	glTexImage2D(GL_TEXTURE_2D, 0, Mode, Surface->w, Surface->h, 0, Mode, GL_UNSIGNED_BYTE, Surface->pixels);
	SDL_FreeSurface(Surface);
	
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	return ID;
}



bool GraphicsInit(void)
{
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
		printf("[!]:\tSDL_Init failed: %s\n", SDL_GetError());
	else
	{	if(IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG)
			printf("[!]:\tIMG_Init failed: %s\n", IMG_GetError());
		else
		{	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
			SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
			//SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
			//SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
			Window = SDL_CreateWindow("Grelk: Swiftness", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowWidth, WindowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
			if(!Window)
				printf("[!]:\tSDL_CreateWindow failed: %s\n", SDL_GetError());
			else
			{	Context = SDL_GL_CreateContext(Window);
				if(!Context)
					printf("[!]:\tSDL_GL_CreateContext failed: %s\n", SDL_GetError());
				else
				{	glewExperimental = GL_TRUE;
					if(glewInit() != GLEW_OK)
						printf("[!]:\tglewInit failed\n");
					else
					{
						if(SDL_GL_SetSwapInterval(-1) == -1)
						{	printf("[!]:\tFailed to set swap interval to -1, %s\n", SDL_GetError());
							if(SDL_GL_SetSwapInterval(1) == -1)
								printf("[!]:\tFailed to set swap interval to 1, %s\n", SDL_GetError());
						}
						glClearColor(0.03, 0.13, 0.16, 1.0);
						glLineWidth(1.0);
						
						MainShader = CreateShader();
						
						TerrainTexture = R_LoadImage("tmpgrfx/Major.png");
						for(size_t i=0; i < et_num; i++)
							EntityTextures[i] = R_LoadImage(EntityTexturePaths[i]);
						
						GLint maxres;
						glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxres);
						printf("Max texture size = %i\n", maxres);
						
						glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
						glEnable(GL_BLEND);
						glDisable(GL_CULL_FACE);
						if(!MainShader)
							printf("[!]:\tFailed to create shader\n");
						else
							return true;
					}
				}
				SDL_DestroyWindow(Window);
			}
			IMG_Quit();
		}
		SDL_Quit();
	}
	return false;
}


void GraphicsQuit()
{
	if(MainShader)
	{	glDeleteProgram(MainShader->program);
		free(MainShader);
	}
	if(Context)
		SDL_GL_DeleteContext(Context);
	if(Window)
		SDL_DestroyWindow(Window);
	SDL_Quit();
}

char* R_FileToStr(const char* path)
{	//	open file
	FILE* file = NULL;
	file = fopen(path,"r");
	if(!file)
	{	printf("[!]:\tR_FileToStr(%s) failed.\n", path);
		return NULL;
	}
	//	get length
	fseek(file, 0, SEEK_END);
	size_t size = ftell(file);
	char* str = malloc(sizeof(char)*(size+1));
	fseek(file, 0, SEEK_SET);
	//	read
	fread(str, size, 1, file);
	str[size] = 0;
	fclose(file);
	return str;
}

/*
float tmpgrfx[12]={
	-0.5,-0.5, 0.0,
	 0.5,-0.5, 0.0,
	 0.0, 0.5, 0.0};
*/

const M3 M3Ident={.m={
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0}};

M3 mat={.m={
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 5.0}};

M3 stmat={.m={
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0}};

M3 camshake={.m={
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 1.0}};

cam Camera =
{	.pos = {.x = -2, .y = -2}
,	.currentScale = 10
,	.targetScale = 1
,	.lead = 0.1
,	.lag = 0.02
,	.actualShake = 0.0
};

bool faceleft = false;

float ms = 0.5;

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	//~@> TODO: Draw the back most obects first
	{
		float cameraMix = powf(Camera.lag, dt);
		Camera.pos = V2Lerp(Camera.tar, Camera.pos, cameraMix*(1-Camera.actualShake));
		cameraMix = powf(0.25, dt);
		Camera.currentScale = (1-cameraMix)*Camera.targetScale + (cameraMix)*Camera.currentScale;
		Camera.actualScale = 0.5+0.5*exp2f(Camera.currentScale);
		camshake = M3Rotate(0.25*Camera.actualShake*(perlin2d(10*time, 0.5f, 1.0, 1.0)-0.5));
		V2 camoff =
		{	.x = 0.4*Camera.actualShake*(perlin2d(10*time,  8.5f, 1.0, 1.0) - 0.5)
		,	.y = 0.4*Camera.actualShake*(perlin2d(10*time, 16.5f, 1.0, 1.0) - 0.5)
		};
		camshake = M3StackTranslate(camoff, camshake);
	}
	//M3Print(camshake);
	/*
	printf("mat:\n");
	for(size_t i = 0; i<16; i++)
	{
		printf("%6.4f,", mat[i]);
		if(i % 4 == 3)
			printf("\n");
	}
	*/
	
	//	initialise shader
	glColor4f(1.0, 1.0, 1.0, 1.0);
	
	glUseProgram(MainShader->program);
	glEnableVertexAttribArray(MainShader->inPos);
	glEnableVertexAttribArray(MainShader->inST);
	
	//~@>	entity drawing
	//	buffers
	glBindBuffer(GL_ARRAY_BUFFER, eVBO);
	glVertexAttribPointer(MainShader->inPos, 3, GL_FLOAT, GL_FALSE, sizeof(rglPoint), (void*)(0));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eIBO);
	glVertexAttribPointer(MainShader->inST, 2, GL_FLOAT, GL_FALSE, sizeof(rglPoint), (void*)(2*sizeof(GLfloat)));
	//	texture
	glBindTexture(GL_TEXTURE_2D, EntityTextures[et_soli]);
	//	geometry matrix
	mat = M3Scale(EntityProperties[et_soli].size);
	//M3Print(mat);
	//M3ID(mat, 0, 0) = 0.5*(16.0/13.0);
	//M3ID(mat, 1, 1) = 0.5*(16.0/13.0);
	mat = M3StackRotation(Player.angle, mat);
	mat = M3StackTranslate(V2Sub(Player.vis, Camera.pos), mat);
	M3ID(mat, 2, 2) = Camera.actualScale;
	mat = M3Mul(M3Mul(aspectmat, camshake), M3Mul(mat, entsqew));
	//M3Print(mat);
	glUniformMatrix3fv(MainShader->matLoc, 1, GL_TRUE, mat.m);
	//	texture matrix
	stmat = M3Ident;
	M3ID(stmat, 0, 0) = 0.5;
	M3ID(stmat, 1, 1) = 0.5;
	if(faceleft)
		M3ID(stmat, 0, 2) = 0.5;
	glUniformMatrix3fv(MainShader->stmatLoc, 1, GL_TRUE, stmat.m);
	//	draw
	glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, NULL);
	
	//~@>	terrain drawing
	//	buffers
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(MainShader->inPos, 3, GL_FLOAT, GL_FALSE, sizeof(rglPoint), (void*)(0));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glVertexAttribPointer(MainShader->inST, 2, GL_FLOAT, GL_FALSE, sizeof(rglPoint), (void*)(2*sizeof(GLfloat)));
	//	geometry matrix
	mat = M3Ident;
	M3ID(mat, 0, 2) = -Camera.pos.x;
	M3ID(mat, 1, 2) = -Camera.pos.y;
	M3ID(mat, 2, 2) =  Camera.actualScale;
	mat = M3Mul(M3Mul(aspectmat, camshake), mat);
	glUniformMatrix3fv(MainShader->matLoc, 1, GL_TRUE, mat.m);
	//	texture matrix
	stmat = M3Ident;
	glUniformMatrix3fv(MainShader->stmatLoc, 1, GL_TRUE, stmat.m);
	//	texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TerrainTexture);
	//	draw
	glDrawElements(GL_TRIANGLES, 16*8*3, GL_UNSIGNED_INT, NULL);
	
	/*
	glDisableVertexAttribArray(MainShader->inPos);
	glDisableVertexAttribArray(MainShader->inST);
	glUseProgram(0);
	*/
	//tmpgrfx
	//	axis lines
	glUseProgram(0);
	glBegin(GL_LINES);
	
	glColor3f ( 1.0, 0.0, 0.0);
	glVertex3f(-1.0, 0.0,-0.5);
	glVertex3f( 1.0, 0.0,-0.5);
	for(float i=0.5*M3ID(aspectmat, 0, 0)/Camera.actualScale; i<1; i+=M3ID(aspectmat, 0, 0)/Camera.actualScale)
	{
		glVertex3f( i, 0.02, -0.5);
		glVertex3f( i,-0.02, -0.5);
		glVertex3f(-i, 0.02, -0.5);
		glVertex3f(-i,-0.02, -0.5);
	}
	
	glColor3f ( 0.0, 1.0, 0.0);
	glVertex3f( 0.0, 1.0,-0.5);
	glVertex3f( 0.0,-1.0,-0.5);
	for(float i=0.5*R3O2*M3ID(aspectmat, 1, 1)/Camera.actualScale; i<1; i += R3O2*M3ID(aspectmat, 1, 1)/Camera.actualScale)
	{
		glVertex3f( 0.02, i, -0.5);
		glVertex3f(-0.02, i, -0.5);
		glVertex3f( 0.02,-i, -0.5);
		glVertex3f(-0.02,-i, -0.5);
	}
	
	{
		static V2 surftest = {};
		static float timer = 0;
		size_t tr = Player.ty, tc = Player.tx;
		TilePoint seg = getSeg(CurrentLevel.tile[16*tr + tc] >> TT_Offset, (int)surftest.y);
		surftest.x += 0.5*dt;
		timer += dt;
		if(surftest.x > seg.halflength)
		{	surftest.x -= seg.halflength;
			surftest.y += 1;
			if(surftest.y >= 2)
				surftest.y = 0;
			seg = getSeg(CurrentLevel.tile[16*tr + tc] >> TT_Offset, (int)surftest.y);
			surftest.x -= seg.halflength;
			//printf("Surface time = %.3f\n", timer);
			timer = 0;
		}
		
		float theta;
		V2 relpos = getVisualPos(surftest, tr, tc, &theta, true, true, false);
		relpos = V2Sub(relpos, Camera.pos);
		relpos.x *= M3ID(aspectmat, 0, 0)/Camera.actualScale;
		relpos.y *= M3ID(aspectmat, 1, 1)/Camera.actualScale;
		
		glColor3f(0.0, 1.0, 0.0);
		glVertex3f(relpos.x-0.1, relpos.y    , 0.0);
		glVertex3f(relpos.x+0.1, relpos.y    , 0.0);
		glColor3f(1.0, 0.0, 0.0);
		glVertex3f(relpos.x    , relpos.y-0.1, 0.0);
		glVertex3f(relpos.x    , relpos.y+0.1, 0.0);
	}
	{
		V2 relpos = V2Scale(Camera.pos, -1/Camera.actualScale);
		relpos.x *= M3ID(aspectmat, 0, 0);
		relpos.y *= M3ID(aspectmat, 1, 1);
		glColor3f(1.0, 0.0, 1.0);
		glVertex3f(relpos.x-0.1, relpos.y    , 0.0);
		glVertex3f(relpos.x+0.1, relpos.y    , 0.0);
		glColor3f(0.0, 1.0, 1.0);
		glVertex3f(relpos.x    , relpos.y-0.1, 0.0);
		glVertex3f(relpos.x    , relpos.y+0.1, 0.0);
	}
	//if(!(Player.flags & sf_lock))
	{
		V2 pv = V2Sub(Player.vis, Camera.pos);
		pv.y += EntityProperties[et_soli].radius;
		//pv.y -= EntityProperties[et_soli].radius;
		pv = V2Scale(pv, 1/Camera.actualScale);
		pv.x *= M3ID(aspectmat, 0, 0);
		pv.y *= M3ID(aspectmat, 1, 1);
		
		V2 dv = {.x=1, .y=1};
		dv = V2Scale(dv, EntityProperties[et_soli].radius/Camera.actualScale);
		dv.x *= M3ID(aspectmat, 0, 0);
		dv.y *= M3ID(aspectmat, 1, 1);
		
		glColor3f(1.0, 0.0, 1.0);
		glVertex3f(pv.x-dv.x, pv.y     , 0.0);
		glVertex3f(pv.x+dv.x, pv.y     , 0.0);
		glColor3f(0.0, 1.0, 1.0);
		glVertex3f(pv.x     , pv.y-dv.y, 0.0);
		glVertex3f(pv.x     , pv.y+dv.y, 0.0);
	}
	glEnd();
	GLenum err = glGetError();
	if(err)
		printf("%04X\n", err);
	
	SDL_GL_SwapWindow(Window);
}
