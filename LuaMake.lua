--[[ lua make ]]

res = "Grelk"
files = {"Main", "Input", "Draw"}


io.write("CC_FLAGS = -Wall -Wextra -g -Og\nLIBS = -lm -lGLU -lGL -lSDL2 -lSDL2_image\n\n")

io.write(res, " :")

for i=1, #files do
	io.write(" ", files[i], ".o")
end
io.write("\n\tgcc")
for i=1, #files do
	io.write(" ", files[i], ".o")
end
io.write(" -o ", res, " $(LIBS)\n")

for i=1, #files do
	io.write("\n", files[i], ".o : ", files[i], ".c\n\tgcc $(CC_Flags) -c ", files[i], ".c -o obj/", files[i], ".o\n")
end

io.write("\nclean:\n\trm -rf *.o\n")
