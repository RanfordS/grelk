#include "Core.h"
#include "World.h"
#include "Draw.h"

Level CurrentLevel;
rglTriangle* Geometry = NULL;
GLuint* GeomIndex = NULL;
GLuint VBO = 0;
GLuint IBO = 0;
/*
void worldInit(void)
{
	TilePointOffset[0] = 0;
	for(TileType i = 1; i<tt_num; i++)
		TilePointOffset[i] = TilePointOffset[i-1] + TilePointSegments[i-1];
}
*/
void loadlevel(size_t id)
{
	if(CurrentLevel.tile)
		free(CurrentLevel.tile);
	
	CurrentLevel.theme = wt_themeless;
	CurrentLevel.width = 16;
	CurrentLevel.height = 8;
	
	CurrentLevel.tile = calloc(CurrentLevel.width*CurrentLevel.height, sizeof(TileType));
	for(size_t c=1; c<15; c++)
	{
		CurrentLevel.tile[16*1 + c] = TileFormat(tt_flat, c&1 ? to_bi : to_b);
		CurrentLevel.tile[16*6 + c] = TileFormat(tt_flat, c&1 ? to_bi : to_b);
	}
	for(size_t r=1; r<7; r++)
	{
		CurrentLevel.tile[16*r + 0] = TileFormat(tt_flat, r&1 ? to_ri : to_li);
		CurrentLevel.tile[16*r + 14] = TileFormat(tt_flat, r&1 ? to_li : to_ri);
	}
	for(size_t r=2; r<6; r++)
	{
		CurrentLevel.tile[16*r + 1] = TileFormat(tt_flat, r&1 ? to_r : to_l);
		CurrentLevel.tile[16*r + 13] = TileFormat(tt_flat, r&1 ? to_l : to_r);
	}
	
	CurrentLevel.tile[16*2 + 6] = TileFormat(tt_flat, to_b);
	CurrentLevel.tile[16*2 + 5] = TileFormat(tt_sharp, to_r);
	CurrentLevel.tile[16*3 + 5] = TileFormat(tt_flat, to_l);
	CurrentLevel.tile[16*3 + 6] = TileFormat(tt_sharp, to_b);
	CurrentLevel.tile[16*3 + 7] = TileFormat(tt_flat, to_r);
	CurrentLevel.tile[16*2 + 7] = TileFormat(tt_sharp, to_l);
	
	/*/
	for(size_t r=0; r < CurrentLevel.height; r++)
		for(size_t c=0; c < CurrentLevel.width; c++)
		{
			CurrentLevel.tile[16*r + c] = TileFormat(r&2 ? tt_flat : tt_sharp, to_fi);
			if((c) & 1)
				CurrentLevel.tile[16*r + c] ^= to_m;
		}
	CurrentLevel.tile[16*2 + 7] &= ~to_m;
	CurrentLevel.tile[16*2 + 7] |= to_li;
	
	CurrentLevel.tile[16*3 + 7] &= ~to_m;
	CurrentLevel.tile[16*3 + 7] |= to_ri;
	
	CurrentLevel.tile[16*3 + 5] &= ~to_m;
	CurrentLevel.tile[16*3 + 5] |= to_li;
	
	CurrentLevel.tile[16*2 + 5] &= ~to_m;
	CurrentLevel.tile[16*2 + 5] |= to_ri;
	
	
	CurrentLevel.tile[16*4 + 7] &= ~to_m;
	CurrentLevel.tile[16*4 + 7] |= to_li;
	CurrentLevel.tile[16*5 + 7] = TileFormat(tt_flat, to_ri);
	
	CurrentLevel.tile[16*4 + 6] = TileFormat(tt_flat, to_fi);
	
	CurrentLevel.tile[16*4 + 5] &= ~to_m;
	CurrentLevel.tile[16*4 + 5] |= to_ri;
	CurrentLevel.tile[16*5 + 5] = TileFormat(tt_flat, to_li);
	
	CurrentLevel.tile[16*2 + 3] = TileFormat(tt_sconr, to_f);
	CurrentLevel.tile[16*2 + 4] = TileFormat(tt_sconr, to_fi);
	
	for(size_t c=8; c<16; c++)
	{
		TileOrient flip = c&1 ? 0 : to_m;
		CurrentLevel.tile[16*2 + c] = TileFormat(tt_sharp, to_f) ^ flip;
		flip ^= to_m;
		CurrentLevel.tile[16*3 + c] = 0;
		CurrentLevel.tile[16*4 + c] = TileFormat(tt_sharp, to_f) ^ flip;
	}
	
	CurrentLevel.tile[16*2 + 15] = TileFormat(tt_sconr, to_li);
	CurrentLevel.tile[16*3 + 15] = TileFormat(tt_sconl, to_ri);
	CurrentLevel.tile[16*3 + 14] = TileFormat(tt_flat, to_r);
	CurrentLevel.tile[16*4 + 14] = TileFormat(tt_flat, to_ri);
	//*/
	Geometry = calloc(CurrentLevel.width*CurrentLevel.height, sizeof(rglTriangle));  // x,y,z,s,t
	GeomIndex = calloc(CurrentLevel.width*CurrentLevel.height*3, sizeof(GLuint));
	
	for(GLuint i = 0; i < CurrentLevel.width*CurrentLevel.height*3; i++)
		GeomIndex[i] = i;
	
	for(size_t r=0; r < CurrentLevel.height; r++)
		for(size_t c=0; c < CurrentLevel.width; c++)
		{
			TileType t = CurrentLevel.tile[16*r + c];
			
			//	all valid tiles must have atleast one orientation bit set
			if(t & to_m)
			{
				size_t gid = 16*r + c;
				uint16_t orient = CurrentLevel.tile[gid] & to_m;
				uint16_t tile = (CurrentLevel.tile[gid] >> 3) - 1;
				GLfloat s = orient / 8.0f;
				GLfloat t = tile / 8.0f;
				
				if((c^r)&1)	//	upness test
				{
					Geometry[gid].A.x = 0.5f*c + 0.5f; Geometry[gid].A.y = (r  )*R3O2;
					Geometry[gid].B.x = 0.5f*c + 1.0f; Geometry[gid].B.y = (r+1)*R3O2;
					Geometry[gid].C.x = 0.5f*c       ; Geometry[gid].C.y = (r+1)*R3O2;
					
					Geometry[gid].A.s = s + (15.0f/128); Geometry[gid].A.t = t + (14.0f/128);
					Geometry[gid].B.s = s + (15.0f/128); Geometry[gid].B.t = t + ( 1.0f/128);
					Geometry[gid].C.s = s + ( 2.0f/128); Geometry[gid].C.t = t + ( 1.0f/128);
				}
				else
				{
					Geometry[gid].A.x = 0.5f*c + 0.5f; Geometry[gid].A.y = (r+1)*R3O2;
					Geometry[gid].B.x = 0.5f*c       ; Geometry[gid].B.y = (r  )*R3O2;
					Geometry[gid].C.x = 0.5f*c + 1.0f; Geometry[gid].C.y = (r  )*R3O2;
					
					Geometry[gid].A.s = s + ( 1.0f/128); Geometry[gid].A.t = t + ( 2.0f/128);
					Geometry[gid].B.s = s + ( 1.0f/128); Geometry[gid].B.t = t + (15.0f/128);
					Geometry[gid].C.s = s + (14.0f/128); Geometry[gid].C.t = t + (15.0f/128);
				}
			}
		}
	//*/
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, 16*8*sizeof(rglTriangle), (GLfloat*)Geometry, GL_STATIC_DRAW);
	free(Geometry);
	
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 16*8*3*sizeof(GLuint), GeomIndex, GL_STATIC_DRAW);
	free(GeomIndex);
}
