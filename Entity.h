#pragma once
#include "Core.h"
#include "World.h"
#include "Draw.h"
//~@>	Entity.h   <@~//

#define MaxEntities 2048

//!	Soli's potential states
typedef enum
{	ss_idle			//!<	Doing an idle animation.
,	ss_standing		//!<	Standing still.
,	ss_breaking		//!<	Resisting momentum.
,	ss_walking		//!<	Walking.
,	ss_running		//!<	Running.
,	ss_jumping		//!<	Positive verical velocity.
,	ss_falling		//!<	Negative verical velocity.
,	ss_attack		//!<	TODO: replace with more specific attack states.
,	ss_stunned		//!<	Stunned.
,	ss_blinking		//!<	Using the short range blink.
,	ss_vaulting		//!<	Vaulting over an edge.
,	ss_wallcling	//!<	Clinging onto a wall.
,	ss_dead			//!<	Waiting to respawn.
} SoliState;

//!	Soli's bit flags.
typedef enum
{	sf_hook		= 1 <<  0	//!<	Augment: Grappling hook.
,	sf_glide	= 1 <<  1	//!<	Augment: Glide (Angel Feathers).
,	sf_vault	= 1 <<  2	//!<	Augment: Ledge Vault.
,	sf_djump	= 1 <<  3	//!<	Augment: Double jump (Rocket Heels).
,	sf_wjump	= 1 <<  4	//!<	Augment: Wall jump (Gripping Sole).
,	sf_bow		= 1 <<  5	//!<	Augment: Ranged attack (Mono Bow).
,	sf_steal	= 1 <<  6	//!<	Augment: Life steal (Vampiric Desire).
,	sf_slam		= 1 <<  7	//!<	Augment: Ground slam (Boom Box).
,	sf_blink	= 1 <<  8	//!<	Augment: Short range blink (Arcanic Delight).
,	sf_ally		= 1 <<  9	//!<	Augment: Friendly minion (Fina).

,	sf_djmpa	= 1 << 10	//!<	Double jump available.
,	sf_lock		= 1 << 15	//!<	Tile lock.
} SoliFlag;

//!	Soli struct.
typedef struct
{	V2 pos;					//!<	Position relative to the current tile. y component is indicates tile segment during tile lock.
	V2 vel;					//!<	Velocity, y component is 0 during tile lock.
	V2 vis;					//!<	The global visual position.
	float angle;
	SoliState state;		//!<	Current state.
	float stateTimer;		//!<	State time.
	size_t tx;				//!<	Tile x. Mostly used when there is a tile lock.
	size_t ty;				//!<	Tile y. Mostly used when there is a tile lock.
	SoliFlag flags;			//!<	Soli bit flags, see SoliFlag enumeration.
	int health;				//!<	The amount of health left.
} Soli;

//!	Entity types.
typedef enum
{	et_soli
,	et_brawler
,	et_num
} EntityType;

typedef struct
{	V2 size;
	float radius;
} EntityProperty;

extern rglQuadrangle EntityGeometry;
extern GLuint EntityTextures[et_num];
extern char EntityTexturePaths[et_num][32];
extern const EntityProperty EntityProperties[et_num];
extern void initentity(void);
extern void ProcessPlayer(void);
extern GLuint eVBO;
extern GLuint eIBO;
extern M3 entsqew;
extern Soli Player;
/*
typedef enum
{	BombCritter
,	Archer
,	Brawler
} EnemyTypes;

typedef enum
{	idle
,	patrolpx
,	patrolpy
,	stunned
,	engaging
,	winding
,	attacking
} EnemyState;

typedef struct
{	EnemyType type;
	double px;
	double py;
	double vx;
	double vy;
		
	
	GDE DrawElement;
} EntityEnemy;
//*/
