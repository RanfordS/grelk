#include "World.h"
#include "Draw.h"

LevelGeometry CurrentLevelGeometry;

void createLevelGeometry(void)
{
	CurrentLevelGeometry.chunksx = 1 + (CurrentLevel.width  - 1) / ChunkSizeX;
	CurrentLevelGeometry.chunksy = 1 + (CurrentLevel.height - 1) / ChunkSizeY;
	CurrentLevelGeometry.VBOs = malloc(CurrentLevelGeometry.chunksx * CurrentLevelGeometry.chunksy * sizeof(GLuint));
	CurrentLevelGeometry.VBOSizes = calloc(CurrentLevelGeometry.chunksx * CurrentLevelGeometry.chunksy, sizeof(size_t));
	
	for(size_t C = 0; C < CurrentLevelGeometry.chunksx; C++)
	{	for(size_t R = 0; R < CurrentLevelGeometry.chunksy; R++)
		{
			size_t ID = CurrentLevelGeometry.chunksx*R + C;
			
			for(size_t c = 0; c < ChunkSizeX; c++)
			{	for(size_t r = 0; r < ChunkSizeY; r++)
				{
					//size_t id = ((R*ChunkSizeY + r)*CurrentLevelGeometry.chunksx + C)*ChunkSizeX + c;
					//	hellish indexing
					size_t id = R*ChunkSizeY + r;
					id *= CurrentLevelGeometry.chunksx;
					id += C;
					id *= ChunkSizeX;
					id += c;
					
					if(CurrentLevel.tile[id] & to_m)
						CurrentLevelGeometry.VBOSizes[ID]++;
			}	}
			
			rglTriangle* data = calloc(CurrentLevelGeometry.VBOSizes[ID], sizeof(rglTriangle));
			size_t gid = 0;
			
			for(size_t c = 0; c < ChunkSizeX; c++)
			{	for(size_t r = 0; r < ChunkSizeY; r++)
				{
					//size_t id = ((R*ChunkSizeY + r)*CurrentLevelGeometry.chunksx + C)*ChunkSizeX + c;
					//	hellish indexing
					size_t id = R*ChunkSizeY + r;
					id *= CurrentLevelGeometry.chunksx;
					id += C;
					id *= ChunkSizeX;
					id += c;
					
					if(CurrentLevel.tile[id] & to_m)
					{
						uint16_t orient = CurrentLevel.tile[gid] & to_m;
						uint16_t tile = (CurrentLevel.tile[gid] >> 3) - 1;
						GLfloat s = orient / 8.0f;
						GLfloat t = tile / 8.0f;
						
						if((c^r)&1)	//	upness test
						{
							data[gid].A.x = 0.5f*c + 0.5f; data[gid].A.y = (r   ) * R3O2;
							data[gid].B.x = 0.5f*c + 1.0f; data[gid].B.y = (r +1) * R3O2;
							data[gid].C.x = 0.5f*c       ; data[gid].C.y = (r +1) * R3O2;
							
							data[gid].A.s = s+(15.0f/128); data[gid].A.t = t+(14.0f/128);
							data[gid].B.s = s+(15.0f/128); data[gid].B.t = t+( 1.0f/128);
							data[gid].C.s = s+( 2.0f/128); data[gid].C.t = t+( 1.0f/128);
						}
						else
						{
							data[gid].A.x = 0.5f*c + 0.5f; data[gid].A.y = (r +1) * R3O2;
							data[gid].B.x = 0.5f*c       ; data[gid].B.y = (r   ) * R3O2;
							data[gid].C.x = 0.5f*c + 1.0f; data[gid].C.y = (r   ) * R3O2;
							
							data[gid].A.s = s+( 1.0f/128); data[gid].A.t = t+( 2.0f/128);
							data[gid].B.s = s+( 1.0f/128); data[gid].B.t = t+(15.0f/128);
							data[gid].C.s = s+(14.0f/128); data[gid].C.t = t+(15.0f/128);
						}
						gid++;
					}
				}
			}
		}
	}
}

void destroyLevelGeometry(void)
{
	
}
